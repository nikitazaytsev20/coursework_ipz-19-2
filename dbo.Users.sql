﻿CREATE TABLE [dbo].[Users] (
    [Id]         INT           NOT NULL,
    [Phone]      VARCHAR (12)  NULL,
    [Password]   NVARCHAR (50) NULL,
    [Email]      NVARCHAR (50) NULL,
    [CardNumber] VARCHAR (16)  NULL,
    [Name]       NVARCHAR (50) NULL,
    [Surname]    NVARCHAR (50) NULL,
    [Patronym]   NVARCHAR (50) NULL,
    [UAH]        FLOAT (53)    NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

