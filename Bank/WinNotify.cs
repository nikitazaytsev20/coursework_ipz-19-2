﻿using System.Drawing;
using System.Windows.Forms;

namespace Bank
{
    public class WinNotify
    {
        static private readonly string iconLocation = @"C:\Users\Nikita\Desktop\Course\Main\Pictures\icon.ico";

        public static void ShowWinNotify(string Title, string Text, int Duration)
        {
            Icon icon = new Icon(iconLocation);
            NotifyIcon notifyIcon = new NotifyIcon();
            notifyIcon.Icon = icon;
            notifyIcon.Visible = true;
            notifyIcon.BalloonTipTitle = Title;
            notifyIcon.BalloonTipText = Text;
            notifyIcon.ShowBalloonTip(Duration);
        }
    }
}