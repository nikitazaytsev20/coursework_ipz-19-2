﻿namespace Bank.Forms
{
    partial class UserHist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserHist));
            this.historyOperation = new System.Windows.Forms.ListBox();
            this.BackToMenu = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.Closebtn = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // historyOperation
            // 
            this.historyOperation.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.historyOperation.FormattingEnabled = true;
            this.historyOperation.HorizontalScrollbar = true;
            this.historyOperation.Location = new System.Drawing.Point(12, 95);
            this.historyOperation.Name = "historyOperation";
            this.historyOperation.Size = new System.Drawing.Size(412, 459);
            this.historyOperation.TabIndex = 7;
            this.historyOperation.SelectedIndexChanged += new System.EventHandler(this.historyOperation_SelectedIndexChanged);
            // 
            // BackToMenu
            // 
            this.BackToMenu.FlatAppearance.BorderSize = 0;
            this.BackToMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BackToMenu.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BackToMenu.ForeColor = System.Drawing.Color.Gray;
            this.BackToMenu.Location = new System.Drawing.Point(90, 594);
            this.BackToMenu.Name = "BackToMenu";
            this.BackToMenu.Size = new System.Drawing.Size(252, 87);
            this.BackToMenu.TabIndex = 8;
            this.BackToMenu.Text = "Повернутися";
            this.BackToMenu.UseVisualStyleBackColor = true;
            this.BackToMenu.Click += new System.EventHandler(this.BackToMenu_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label.Location = new System.Drawing.Point(54, 36);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(324, 38);
            this.label.TabIndex = 9;
            this.label.Text = "Історія операцій";
            // 
            // Closebtn
            // 
            this.Closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Closebtn.Image = global::Bank.Properties.Resources.Close;
            this.Closebtn.Location = new System.Drawing.Point(395, 11);
            this.Closebtn.Margin = new System.Windows.Forms.Padding(2);
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.Size = new System.Drawing.Size(30, 32);
            this.Closebtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Closebtn.TabIndex = 19;
            this.Closebtn.TabStop = false;
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox7.Location = new System.Drawing.Point(-3, 727);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(443, 108);
            this.pictureBox7.TabIndex = 63;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox6.Location = new System.Drawing.Point(-3, -106);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(443, 110);
            this.pictureBox6.TabIndex = 62;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox5.Location = new System.Drawing.Point(-3, -52);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(8, 836);
            this.pictureBox5.TabIndex = 61;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox8.Location = new System.Drawing.Point(432, -54);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(8, 836);
            this.pictureBox8.TabIndex = 60;
            this.pictureBox8.TabStop = false;
            // 
            // UserHist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(436, 731);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.label);
            this.Controls.Add(this.BackToMenu);
            this.Controls.Add(this.historyOperation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserHist";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UserHist";
            this.Load += new System.EventHandler(this.UserHist_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox historyOperation;
        private System.Windows.Forms.Button BackToMenu;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.PictureBox Closebtn;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox8;
    }
}