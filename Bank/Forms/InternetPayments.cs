﻿using HistoryClass;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;

namespace Bank.Forms
{
    public partial class InternetPayments : Form
    {
        UserTransactions user = new UserTransactions();
        float amount = 0;
        public long login;
        MainMenu mainMenu = new MainMenu();


        public InternetPayments()
        {
            InitializeComponent();
        }



        private void InternetPayments_Load(object sender, EventArgs e)
        {
            QiwiPanel.Hide();
            PayPalPanel.Hide();
            user.UserConnection(login);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            InternetPayments home = new InternetPayments();
            mainMenu.login = login;
            this.Close();
            mainMenu.Show();
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Qiwi_Click(object sender, EventArgs e)
        {
            QiwiPanel.Show();
        }

        private void PayPal_Click(object sender, EventArgs e)
        {
            PayPalPanel.Show();
        }

        private void QiwiBack_Click(object sender, EventArgs e)
        {
            QiwiPanel.Hide();
        }

        private void IdQiwi_TextChanged(object sender, EventArgs e)
        {

        }

        private void IdQiwi_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(IdQiwi.Text, @"^\d{12}$", RegexOptions.Compiled))
            {

                MessageBox.Show("Невірно введений Id! Перевірте будь ласка та введіть ще раз!");

            }

        }

        private void AmountQiwi_TextChanged(object sender, EventArgs e)
        {
            AmountQiwi.MaxLength = 8;
        }

        private void AmountQiwi_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(AmountQiwi.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled))
            {
                AmountQiwi.Clear();
                AmountQiwi.Focus();
                MessageBox.Show("Введена сума не відповідає формату! Перевірте та введіть ще раз.");

            }
        }

        private void payQiwi_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(AmountQiwi.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && Regex.IsMatch(IdQiwi.Text, @"^\d{12}$", RegexOptions.Compiled))
            {
                string qiwiId = IdQiwi.Text;
                amount = float.Parse(AmountQiwi.Text.Replace(".", ","));

                if (user.uah >= amount)
                {
                    this.Visible = false;
                    QiwiPanel.Hide();
                    user.uah -= amount;
                    this.Opacity = 1;
                    user.UserTransfer(login, user.uah);
                    Close();
                    History.AddToHistory(user.userCard, $"{DateTime.Now.ToShortDateString()} | {amount} – Поповнення гаманця Qiwi {qiwiId}");
                    WinNotify.ShowWinNotify("Оплата поповнення рахунку QIWI", $"Ви успішно поповнили рахунок QIWI {qiwiId} в розмірі {amount}₴\nВаш поточний рахунок: {user.uah:0.##}₴", 5000);

                    mainMenu.login = login;
                    mainMenu.Show();
                }
                else
                {
                    MessageBox.Show($"{user.userName}, у вас недостатньо коштів для оплати.", "Недостатньо коштів", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    amount = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Ви ввели невірні дані.\nПеревірте та введіть ще раз.", "Помилка формату", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void QiwiClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void IdPayPal_TextChanged(object sender, EventArgs e)
        {

        }

        private void IdPayPal_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(IdPayPal.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                IdPayPal.Clear();
                IdPayPal.Focus();
                MessageBox.Show("Введений ID не відповідає формату! Введіть ваш Email.");

            }
        }

        private void amountPayPal_TextChanged(object sender, EventArgs e)
        {
            amountPayPal.MaxLength = 8;
        }

        private void PayPalPay_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(amountPayPal.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && Regex.IsMatch(IdPayPal.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                string paypalId = IdPayPal.Text;
                amount = float.Parse(amountPayPal.Text.Replace(".", ","));

                if (user.uah >= amount)
                {
                    this.Visible = false;
                    PayPalPanel.Hide();
                    user.uah -= amount;
                    this.Opacity = 1;
                    user.UserTransfer(login, user.uah);
                    Close();
                    History.AddToHistory(user.userCard, $"{DateTime.Now.ToShortDateString()} | {amount} – Поповнення гаманця PayPal {paypalId}");
                    WinNotify.ShowWinNotify("Оплата поповнення рахунку PayPal", $"Ви успішно поповнили рахунок PayPal {paypalId} в розмірі {amount}₴\nВаш поточний рахунок: {user.uah:0.##}₴", 5000);

                    mainMenu.login = login;
                    mainMenu.Show();
                }
                else
                {
                    MessageBox.Show($"{user.userName}, у вас недостатньо коштів для оплати.", "Недостатньо коштів", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    amount = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Ви ввели невірні дані.\nПеревірте та введіть ще раз.", "Помилка формату", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void amountPayPal_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(amountPayPal.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled))
            {
                amountPayPal.Clear();
                amountPayPal.Focus();
                MessageBox.Show("Введена сума не відповідає формату! Перевірте та введіть ще раз.");

            }
        }

        private void PayPalBack_Click(object sender, EventArgs e)
        {
            PayPalPanel.Hide();
        }

        private void PayPalClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void IdPayPal_Click(object sender, EventArgs e)
        {
            IdPayPal.Clear();
        }

        private void amountPayPal_Click(object sender, EventArgs e)
        {
            amountPayPal.Clear();
        }

        private void AmountQiwi_Click(object sender, EventArgs e)
        {
            AmountQiwi.Clear();
        }

        private void IdQiwi_Click(object sender, EventArgs e)
        {
            IdQiwi.Clear();
        }

        private void QiwiPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
