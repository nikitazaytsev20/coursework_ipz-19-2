﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Bank.Forms
{
    public partial class UserHist : Form
    {
        public string card;

        public string name;

        public string surname;

        public string patronym;

        MainMenu mainMenu = new MainMenu();

        public long login;

        public List<string> historyList = new List<string>();
        public UserHist()
        {
            InitializeComponent();
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            mainMenu.login = login;
            this.Close();
            mainMenu.Opacity = 1;
        }

        private void historyOperation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void UserHist_Load(object sender, EventArgs e)
        {
            string path = $@"C:\Users\Nikita\Desktop\Course\Main\History\history_of_user#{card}.txt";
            string[] operations = File.ReadAllLines(path, System.Text.Encoding.Default);
            foreach (string operation in operations)
            {
                historyOperation.Items.Add(operation);
            }
        }

        private void BackToMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
