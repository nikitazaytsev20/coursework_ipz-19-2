﻿namespace Bank
{
    partial class RegistrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistrationForm));
            this.phonenumber = new System.Windows.Forms.TextBox();
            this.cardNumber = new System.Windows.Forms.TextBox();
            this.uEmail = new System.Windows.Forms.TextBox();
            this.uPassword = new System.Windows.Forms.TextBox();
            this.uName = new System.Windows.Forms.TextBox();
            this.uSurname = new System.Windows.Forms.TextBox();
            this.uPatronym = new System.Windows.Forms.TextBox();
            this.Regbtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.eyepict = new System.Windows.Forms.PictureBox();
            this.Supportbtn = new System.Windows.Forms.PictureBox();
            this.Closebtn = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PatronymCheck = new System.Windows.Forms.PictureBox();
            this.NameCheck = new System.Windows.Forms.PictureBox();
            this.SurnameCheck = new System.Windows.Forms.PictureBox();
            this.EmailCheck = new System.Windows.Forms.PictureBox();
            this.PasswordCheck = new System.Windows.Forms.PictureBox();
            this.CardCheck = new System.Windows.Forms.PictureBox();
            this.PhoneCheck = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.eyepict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Supportbtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PatronymCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurnameCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CardCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // phonenumber
            // 
            this.phonenumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.phonenumber.Location = new System.Drawing.Point(29, 123);
            this.phonenumber.Margin = new System.Windows.Forms.Padding(2);
            this.phonenumber.Name = "phonenumber";
            this.phonenumber.Size = new System.Drawing.Size(233, 33);
            this.phonenumber.TabIndex = 0;
            this.phonenumber.TextChanged += new System.EventHandler(this.phonenumber_TextChanged);
            // 
            // cardNumber
            // 
            this.cardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.cardNumber.Location = new System.Drawing.Point(29, 186);
            this.cardNumber.Margin = new System.Windows.Forms.Padding(2);
            this.cardNumber.Name = "cardNumber";
            this.cardNumber.Size = new System.Drawing.Size(233, 33);
            this.cardNumber.TabIndex = 2;
            this.cardNumber.TextChanged += new System.EventHandler(this.cardNumber_TextChanged);
            // 
            // uEmail
            // 
            this.uEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.uEmail.Location = new System.Drawing.Point(29, 309);
            this.uEmail.Margin = new System.Windows.Forms.Padding(2);
            this.uEmail.Name = "uEmail";
            this.uEmail.Size = new System.Drawing.Size(233, 33);
            this.uEmail.TabIndex = 3;
            this.uEmail.TextChanged += new System.EventHandler(this.uEmail_TextChanged);
            // 
            // uPassword
            // 
            this.uPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.uPassword.Location = new System.Drawing.Point(29, 243);
            this.uPassword.Margin = new System.Windows.Forms.Padding(2);
            this.uPassword.Name = "uPassword";
            this.uPassword.Size = new System.Drawing.Size(198, 33);
            this.uPassword.TabIndex = 4;
            this.uPassword.UseSystemPasswordChar = true;
            this.uPassword.TextChanged += new System.EventHandler(this.uPassword_TextChanged);
            // 
            // uName
            // 
            this.uName.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.uName.Location = new System.Drawing.Point(411, 186);
            this.uName.Margin = new System.Windows.Forms.Padding(2);
            this.uName.Name = "uName";
            this.uName.Size = new System.Drawing.Size(233, 33);
            this.uName.TabIndex = 5;
            this.uName.TextChanged += new System.EventHandler(this.uName_TextChanged);
            // 
            // uSurname
            // 
            this.uSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.uSurname.Location = new System.Drawing.Point(411, 123);
            this.uSurname.Margin = new System.Windows.Forms.Padding(2);
            this.uSurname.Name = "uSurname";
            this.uSurname.Size = new System.Drawing.Size(233, 33);
            this.uSurname.TabIndex = 6;
            this.uSurname.TextChanged += new System.EventHandler(this.uSurname_TextChanged);
            // 
            // uPatronym
            // 
            this.uPatronym.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.uPatronym.Location = new System.Drawing.Point(411, 243);
            this.uPatronym.Margin = new System.Windows.Forms.Padding(2);
            this.uPatronym.Name = "uPatronym";
            this.uPatronym.Size = new System.Drawing.Size(233, 33);
            this.uPatronym.TabIndex = 7;
            this.uPatronym.TextChanged += new System.EventHandler(this.uPatronym_TextChanged);
            // 
            // Regbtn
            // 
            this.Regbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Regbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Regbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Regbtn.Location = new System.Drawing.Point(219, 362);
            this.Regbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Regbtn.Name = "Regbtn";
            this.Regbtn.Size = new System.Drawing.Size(273, 77);
            this.Regbtn.TabIndex = 8;
            this.Regbtn.Text = "Зареєструватися";
            this.Regbtn.UseVisualStyleBackColor = true;
            this.Regbtn.Click += new System.EventHandler(this.Regbtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.label1.Location = new System.Drawing.Point(220, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 55);
            this.label1.TabIndex = 20;
            this.label1.Text = "Реєстрація";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Window;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label2.Location = new System.Drawing.Point(20, 110);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 22);
            this.label2.TabIndex = 21;
            this.label2.Text = "Номер телефону";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Window;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Location = new System.Drawing.Point(20, 171);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 22);
            this.label3.TabIndex = 22;
            this.label3.Text = "Номер карти";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Window;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label4.Location = new System.Drawing.Point(20, 229);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 22);
            this.label4.TabIndex = 23;
            this.label4.Text = "Пароль";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Window;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label5.Location = new System.Drawing.Point(20, 295);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 22);
            this.label5.TabIndex = 24;
            this.label5.Text = "Email";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Window;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label6.Location = new System.Drawing.Point(402, 110);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 22);
            this.label6.TabIndex = 25;
            this.label6.Text = "Фамілія";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Window;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label7.Location = new System.Drawing.Point(402, 171);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 22);
            this.label7.TabIndex = 26;
            this.label7.Text = "Ім\'я";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.Window;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label8.Location = new System.Drawing.Point(402, 229);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 22);
            this.label8.TabIndex = 27;
            this.label8.Text = "По батькові";
            // 
            // eyepict
            // 
            this.eyepict.BackColor = System.Drawing.SystemColors.Window;
            this.eyepict.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.eyepict.Image = global::Bank.Properties.Resources.hiddeneye;
            this.eyepict.Location = new System.Drawing.Point(231, 243);
            this.eyepict.Margin = new System.Windows.Forms.Padding(2);
            this.eyepict.Name = "eyepict";
            this.eyepict.Size = new System.Drawing.Size(30, 32);
            this.eyepict.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.eyepict.TabIndex = 28;
            this.eyepict.TabStop = false;
            this.eyepict.Click += new System.EventHandler(this.eyepict_Click);
            // 
            // Supportbtn
            // 
            this.Supportbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Supportbtn.Image = global::Bank.Properties.Resources.Support;
            this.Supportbtn.Location = new System.Drawing.Point(666, 473);
            this.Supportbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Supportbtn.Name = "Supportbtn";
            this.Supportbtn.Size = new System.Drawing.Size(30, 32);
            this.Supportbtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Supportbtn.TabIndex = 19;
            this.Supportbtn.TabStop = false;
            this.Supportbtn.Click += new System.EventHandler(this.Supportbtn_Click);
            // 
            // Closebtn
            // 
            this.Closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Closebtn.Image = global::Bank.Properties.Resources.Close;
            this.Closebtn.Location = new System.Drawing.Point(9, 473);
            this.Closebtn.Margin = new System.Windows.Forms.Padding(2);
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.Size = new System.Drawing.Size(30, 32);
            this.Closebtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Closebtn.TabIndex = 18;
            this.Closebtn.TabStop = false;
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pictureBox2.Location = new System.Drawing.Point(-4, 466);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(718, 2);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Bank.Properties.Resources.back;
            this.pictureBox1.Location = new System.Drawing.Point(9, 10);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 41);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PatronymCheck
            // 
            this.PatronymCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PatronymCheck.Location = new System.Drawing.Point(647, 243);
            this.PatronymCheck.Margin = new System.Windows.Forms.Padding(2);
            this.PatronymCheck.Name = "PatronymCheck";
            this.PatronymCheck.Size = new System.Drawing.Size(30, 32);
            this.PatronymCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PatronymCheck.TabIndex = 15;
            this.PatronymCheck.TabStop = false;
            // 
            // NameCheck
            // 
            this.NameCheck.BackColor = System.Drawing.SystemColors.Window;
            this.NameCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.NameCheck.Location = new System.Drawing.Point(647, 186);
            this.NameCheck.Margin = new System.Windows.Forms.Padding(2);
            this.NameCheck.Name = "NameCheck";
            this.NameCheck.Size = new System.Drawing.Size(30, 32);
            this.NameCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.NameCheck.TabIndex = 14;
            this.NameCheck.TabStop = false;
            // 
            // SurnameCheck
            // 
            this.SurnameCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.SurnameCheck.Location = new System.Drawing.Point(647, 123);
            this.SurnameCheck.Margin = new System.Windows.Forms.Padding(2);
            this.SurnameCheck.Name = "SurnameCheck";
            this.SurnameCheck.Size = new System.Drawing.Size(30, 32);
            this.SurnameCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SurnameCheck.TabIndex = 13;
            this.SurnameCheck.TabStop = false;
            // 
            // EmailCheck
            // 
            this.EmailCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.EmailCheck.Location = new System.Drawing.Point(266, 309);
            this.EmailCheck.Margin = new System.Windows.Forms.Padding(2);
            this.EmailCheck.Name = "EmailCheck";
            this.EmailCheck.Size = new System.Drawing.Size(30, 32);
            this.EmailCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.EmailCheck.TabIndex = 12;
            this.EmailCheck.TabStop = false;
            // 
            // PasswordCheck
            // 
            this.PasswordCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PasswordCheck.Location = new System.Drawing.Point(266, 243);
            this.PasswordCheck.Margin = new System.Windows.Forms.Padding(2);
            this.PasswordCheck.Name = "PasswordCheck";
            this.PasswordCheck.Size = new System.Drawing.Size(30, 32);
            this.PasswordCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PasswordCheck.TabIndex = 11;
            this.PasswordCheck.TabStop = false;
            // 
            // CardCheck
            // 
            this.CardCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.CardCheck.Location = new System.Drawing.Point(266, 186);
            this.CardCheck.Margin = new System.Windows.Forms.Padding(2);
            this.CardCheck.Name = "CardCheck";
            this.CardCheck.Size = new System.Drawing.Size(30, 32);
            this.CardCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CardCheck.TabIndex = 10;
            this.CardCheck.TabStop = false;
            // 
            // PhoneCheck
            // 
            this.PhoneCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PhoneCheck.Location = new System.Drawing.Point(266, 123);
            this.PhoneCheck.Margin = new System.Windows.Forms.Padding(2);
            this.PhoneCheck.Name = "PhoneCheck";
            this.PhoneCheck.Size = new System.Drawing.Size(30, 32);
            this.PhoneCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PhoneCheck.TabIndex = 9;
            this.PhoneCheck.TabStop = false;
            this.PhoneCheck.Click += new System.EventHandler(this.PhoneCheck_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox6.Location = new System.Drawing.Point(-4, -208);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(443, 156);
            this.pictureBox6.TabIndex = 62;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox5.Location = new System.Drawing.Point(-4, -156);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(8, 836);
            this.pictureBox5.TabIndex = 61;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox3.Location = new System.Drawing.Point(701, -170);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(8, 836);
            this.pictureBox3.TabIndex = 63;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox4.Location = new System.Drawing.Point(-19, 504);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(742, 662);
            this.pictureBox4.TabIndex = 64;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox7.Location = new System.Drawing.Point(-19, -80);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(742, 84);
            this.pictureBox7.TabIndex = 65;
            this.pictureBox7.TabStop = false;
            // 
            // RegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(705, 508);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.eyepict);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Supportbtn);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PatronymCheck);
            this.Controls.Add(this.NameCheck);
            this.Controls.Add(this.SurnameCheck);
            this.Controls.Add(this.EmailCheck);
            this.Controls.Add(this.PasswordCheck);
            this.Controls.Add(this.CardCheck);
            this.Controls.Add(this.PhoneCheck);
            this.Controls.Add(this.Regbtn);
            this.Controls.Add(this.uPatronym);
            this.Controls.Add(this.uSurname);
            this.Controls.Add(this.uName);
            this.Controls.Add(this.uPassword);
            this.Controls.Add(this.uEmail);
            this.Controls.Add(this.cardNumber);
            this.Controls.Add(this.phonenumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "RegistrationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.RegistrationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.eyepict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Supportbtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PatronymCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurnameCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CardCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox phonenumber;
        private System.Windows.Forms.TextBox cardNumber;
        private System.Windows.Forms.TextBox uEmail;
        private System.Windows.Forms.TextBox uPassword;
        private System.Windows.Forms.TextBox uName;
        private System.Windows.Forms.TextBox uSurname;
        private System.Windows.Forms.TextBox uPatronym;
        private System.Windows.Forms.Button Regbtn;
        private System.Windows.Forms.PictureBox PhoneCheck;
        private System.Windows.Forms.PictureBox CardCheck;
        private System.Windows.Forms.PictureBox PasswordCheck;
        private System.Windows.Forms.PictureBox EmailCheck;
        private System.Windows.Forms.PictureBox SurnameCheck;
        private System.Windows.Forms.PictureBox NameCheck;
        private System.Windows.Forms.PictureBox PatronymCheck;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox Closebtn;
        private System.Windows.Forms.PictureBox Supportbtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox eyepict;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox7;
    }
}