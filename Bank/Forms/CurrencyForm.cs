﻿using Currency;
using System;
using System.Windows.Forms;



namespace Bank.Forms
{
    public partial class CurrencyForm : Form
    {
        public CurrencyForm()
        {
            InitializeComponent();
            currencyToConvert.SelectedIndex = 0;
            CurrencyToConvert1.SelectedIndex = 0;
        }
        CurrencyClass Currency = new CurrencyClass();
        MainMenu mainMenu = new MainMenu();

        public long login;
        private void CurrencyForm_Load(object sender, EventArgs e)
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                today.Text = DateTime.Now.ToShortDateString();
                buyUSD.Text = Currency.toBuyUsd.ToString();
                sellUsd.Text = Currency.toSellUsd.ToString();
                buyEur.Text = Currency.toBuyEur.ToString();
                sellEur.Text = Currency.toSellEur.ToString();
                buyRub.Text = Currency.toBuyRub.ToString();
                sellRub.Text = Currency.toSellRub.ToString();
            }
            else
            {
                MessageBox.Show("Помилка під'єднання до серверу", "Помилка з'єднання", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                mainMenu.login = login;
                mainMenu.Show();
                return;
            }
        }

        private void SellUAH_TextChanged(object sender, EventArgs e)
        {
            double value;
            try
            {
                if (currencyToConvert.SelectedIndex == 0)
                {
                    value = Math.Round(double.Parse(SellUAH.Text.Replace(".", ",")));
                    BuyCurr.Text = (value / Currency.toBuyUsd).ToString("F2");
                }
                else if (currencyToConvert.SelectedIndex == 1)
                {
                    value = Math.Round(double.Parse(SellUAH.Text.Replace(".", ",")));
                    BuyCurr.Text = (value / Currency.toBuyEur).ToString("F2");
                }
                else if (currencyToConvert.SelectedIndex == 2)
                {
                    value = Math.Round(double.Parse(SellUAH.Text.Replace(".", ",")));
                    BuyCurr.Text = (value / Currency.toBuyRub).ToString("F2");
                }
            }
            catch (FormatException)
            {
                BuyCurr.Text = "0";
            }
        }

        private void SellCur_TextChanged(object sender, EventArgs e)
        {
            decimal value;
            try
            {
                if (CurrencyToConvert1.SelectedIndex == 0)
                {
                    value = Math.Round(decimal.Parse(SellCur.Text.Replace(".", ",")));
                    BuyUAH.Text = (value * (decimal)Currency.toSellUsd).ToString("F2");
                }
                else if (CurrencyToConvert1.SelectedIndex == 1)
                {
                    value = Math.Round(decimal.Parse(SellCur.Text.Replace(".", ",")));
                    BuyUAH.Text = (value * (decimal)Currency.toSellEur).ToString("F2");
                }
                else if (CurrencyToConvert1.SelectedIndex == 2)
                {
                    value = Math.Round(decimal.Parse(SellCur.Text.Replace(".", ",")));
                    BuyUAH.Text = (value * (decimal)Currency.toSellRub).ToString("F2");
                }
            }
            catch (FormatException)
            {
               
            }
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            mainMenu.login = login;
            this.Close();
            mainMenu.Show();
        }

        private void UahToCurr_Click(object sender, EventArgs e)
        {

        }

        private void currencyToConvert_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void currencyToConvert_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SellUAH.Clear();
        }

        private void CurrencyToConvert1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SellCur.Clear();
        }

        private void CurrencyToConvert1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

