﻿namespace Bank.Forms
{
    partial class CurrencyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CurrencyForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sellRub = new System.Windows.Forms.Label();
            this.buyRub = new System.Windows.Forms.Label();
            this.sellEur = new System.Windows.Forms.Label();
            this.buyEur = new System.Windows.Forms.Label();
            this.sellUsd = new System.Windows.Forms.Label();
            this.buyUSD = new System.Windows.Forms.Label();
            this.text = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SellUAH = new System.Windows.Forms.TextBox();
            this.currencyToConvert = new System.Windows.Forms.ComboBox();
            this.BuyCurr = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CurrencyToConvert1 = new System.Windows.Forms.ComboBox();
            this.SellCur = new System.Windows.Forms.TextBox();
            this.BuyUAH = new System.Windows.Forms.TextBox();
            this.today = new System.Windows.Forms.Label();
            this.Closebtn = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.exgStripes = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exgStripes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(125, 182);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 37);
            this.label1.TabIndex = 1;
            this.label1.Text = "Курс валют";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(32, 390);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 23);
            this.label7.TabIndex = 22;
            this.label7.Text = "Рубль";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(31, 352);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 23);
            this.label6.TabIndex = 21;
            this.label6.Text = "Євро";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(32, 312);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 23);
            this.label5.TabIndex = 20;
            this.label5.Text = "Доллар";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(22, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 25);
            this.label4.TabIndex = 19;
            this.label4.Text = "Валюта";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(302, 273);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 25);
            this.label3.TabIndex = 24;
            this.label3.Text = "Продаж";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(174, 273);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 25);
            this.label2.TabIndex = 23;
            this.label2.Text = "Купівля";
            // 
            // sellRub
            // 
            this.sellRub.AutoSize = true;
            this.sellRub.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sellRub.Location = new System.Drawing.Point(313, 390);
            this.sellRub.Name = "sellRub";
            this.sellRub.Size = new System.Drawing.Size(22, 23);
            this.sellRub.TabIndex = 30;
            this.sellRub.Text = "2";
            // 
            // buyRub
            // 
            this.buyRub.AutoSize = true;
            this.buyRub.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buyRub.Location = new System.Drawing.Point(185, 390);
            this.buyRub.Name = "buyRub";
            this.buyRub.Size = new System.Drawing.Size(22, 23);
            this.buyRub.TabIndex = 29;
            this.buyRub.Text = "1";
            // 
            // sellEur
            // 
            this.sellEur.AutoSize = true;
            this.sellEur.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sellEur.Location = new System.Drawing.Point(313, 352);
            this.sellEur.Name = "sellEur";
            this.sellEur.Size = new System.Drawing.Size(22, 23);
            this.sellEur.TabIndex = 28;
            this.sellEur.Text = "2";
            // 
            // buyEur
            // 
            this.buyEur.AutoSize = true;
            this.buyEur.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buyEur.Location = new System.Drawing.Point(185, 352);
            this.buyEur.Name = "buyEur";
            this.buyEur.Size = new System.Drawing.Size(22, 23);
            this.buyEur.TabIndex = 27;
            this.buyEur.Text = "1";
            // 
            // sellUsd
            // 
            this.sellUsd.AutoSize = true;
            this.sellUsd.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sellUsd.Location = new System.Drawing.Point(313, 312);
            this.sellUsd.Name = "sellUsd";
            this.sellUsd.Size = new System.Drawing.Size(22, 23);
            this.sellUsd.TabIndex = 26;
            this.sellUsd.Text = "2";
            // 
            // buyUSD
            // 
            this.buyUSD.AutoSize = true;
            this.buyUSD.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buyUSD.Location = new System.Drawing.Point(185, 312);
            this.buyUSD.Name = "buyUSD";
            this.buyUSD.Size = new System.Drawing.Size(22, 23);
            this.buyUSD.TabIndex = 25;
            this.buyUSD.Text = "1";
            // 
            // text
            // 
            this.text.AutoSize = true;
            this.text.Font = new System.Drawing.Font("Verdana", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text.Location = new System.Drawing.Point(109, 432);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(222, 25);
            this.text.TabIndex = 31;
            this.text.Text = "Конвертор валют";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(65, 467);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(313, 23);
            this.label8.TabIndex = 32;
            this.label8.Text = "From UAH to USD, EUR, RUB";
            // 
            // SellUAH
            // 
            this.SellUAH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SellUAH.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SellUAH.Location = new System.Drawing.Point(136, 517);
            this.SellUAH.Name = "SellUAH";
            this.SellUAH.Size = new System.Drawing.Size(98, 34);
            this.SellUAH.TabIndex = 33;
            this.SellUAH.TextChanged += new System.EventHandler(this.SellUAH_TextChanged);
            // 
            // currencyToConvert
            // 
            this.currencyToConvert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currencyToConvert.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.currencyToConvert.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.currencyToConvert.FormattingEnabled = true;
            this.currencyToConvert.Items.AddRange(new object[] {
            "USD",
            "EUR",
            "RUB"});
            this.currencyToConvert.Location = new System.Drawing.Point(40, 516);
            this.currencyToConvert.Name = "currencyToConvert";
            this.currencyToConvert.Size = new System.Drawing.Size(92, 33);
            this.currencyToConvert.TabIndex = 35;
            this.currencyToConvert.TabStop = false;
            this.currencyToConvert.SelectedIndexChanged += new System.EventHandler(this.currencyToConvert_SelectedIndexChanged);
            this.currencyToConvert.SelectionChangeCommitted += new System.EventHandler(this.currencyToConvert_SelectionChangeCommitted);
            // 
            // BuyCurr
            // 
            this.BuyCurr.BackColor = System.Drawing.SystemColors.Window;
            this.BuyCurr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BuyCurr.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BuyCurr.Location = new System.Drawing.Point(280, 514);
            this.BuyCurr.Name = "BuyCurr";
            this.BuyCurr.ReadOnly = true;
            this.BuyCurr.Size = new System.Drawing.Size(98, 27);
            this.BuyCurr.TabIndex = 36;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(65, 568);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(313, 23);
            this.label9.TabIndex = 37;
            this.label9.Text = "From USD, EUR, RUB to UAH";
            // 
            // CurrencyToConvert1
            // 
            this.CurrencyToConvert1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CurrencyToConvert1.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CurrencyToConvert1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.CurrencyToConvert1.FormattingEnabled = true;
            this.CurrencyToConvert1.Items.AddRange(new object[] {
            "USD",
            "EUR",
            "RUB"});
            this.CurrencyToConvert1.Location = new System.Drawing.Point(40, 623);
            this.CurrencyToConvert1.Name = "CurrencyToConvert1";
            this.CurrencyToConvert1.Size = new System.Drawing.Size(90, 33);
            this.CurrencyToConvert1.TabIndex = 38;
            this.CurrencyToConvert1.TabStop = false;
            this.CurrencyToConvert1.SelectedIndexChanged += new System.EventHandler(this.CurrencyToConvert1_SelectedIndexChanged);
            this.CurrencyToConvert1.SelectionChangeCommitted += new System.EventHandler(this.CurrencyToConvert1_SelectionChangeCommitted);
            // 
            // SellCur
            // 
            this.SellCur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SellCur.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SellCur.Location = new System.Drawing.Point(136, 624);
            this.SellCur.Name = "SellCur";
            this.SellCur.Size = new System.Drawing.Size(98, 34);
            this.SellCur.TabIndex = 39;
            this.SellCur.TextChanged += new System.EventHandler(this.SellCur_TextChanged);
            // 
            // BuyUAH
            // 
            this.BuyUAH.BackColor = System.Drawing.SystemColors.Window;
            this.BuyUAH.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BuyUAH.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BuyUAH.Location = new System.Drawing.Point(280, 624);
            this.BuyUAH.Name = "BuyUAH";
            this.BuyUAH.ReadOnly = true;
            this.BuyUAH.Size = new System.Drawing.Size(98, 27);
            this.BuyUAH.TabIndex = 41;
            // 
            // today
            // 
            this.today.AutoSize = true;
            this.today.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.today.Location = new System.Drawing.Point(151, 230);
            this.today.Name = "today";
            this.today.Size = new System.Drawing.Size(0, 18);
            this.today.TabIndex = 44;
            // 
            // Closebtn
            // 
            this.Closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Closebtn.Image = global::Bank.Properties.Resources.Close;
            this.Closebtn.Location = new System.Drawing.Point(386, 11);
            this.Closebtn.Margin = new System.Windows.Forms.Padding(2);
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.Size = new System.Drawing.Size(39, 41);
            this.Closebtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Closebtn.TabIndex = 43;
            this.Closebtn.TabStop = false;
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::Bank.Properties.Resources.back;
            this.pictureBox3.Location = new System.Drawing.Point(11, 11);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(38, 41);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 42;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(240, 622);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(34, 34);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // exgStripes
            // 
            this.exgStripes.Image = ((System.Drawing.Image)(resources.GetObject("exgStripes.Image")));
            this.exgStripes.Location = new System.Drawing.Point(240, 514);
            this.exgStripes.Name = "exgStripes";
            this.exgStripes.Size = new System.Drawing.Size(34, 34);
            this.exgStripes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exgStripes.TabIndex = 34;
            this.exgStripes.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Bank.Properties.Resources.Planet;
            this.pictureBox1.Location = new System.Drawing.Point(154, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox4.Location = new System.Drawing.Point(432, -7);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(8, 741);
            this.pictureBox4.TabIndex = 45;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox5.Location = new System.Drawing.Point(-3, -5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(8, 741);
            this.pictureBox5.TabIndex = 46;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox6.Location = new System.Drawing.Point(-3, -57);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(443, 61);
            this.pictureBox6.TabIndex = 47;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox7.Location = new System.Drawing.Point(-3, 726);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(443, 61);
            this.pictureBox7.TabIndex = 48;
            this.pictureBox7.TabStop = false;
            // 
            // CurrencyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(436, 731);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.today);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.BuyUAH);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.SellCur);
            this.Controls.Add(this.CurrencyToConvert1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.BuyCurr);
            this.Controls.Add(this.currencyToConvert);
            this.Controls.Add(this.exgStripes);
            this.Controls.Add(this.SellUAH);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.text);
            this.Controls.Add(this.sellRub);
            this.Controls.Add(this.buyRub);
            this.Controls.Add(this.sellEur);
            this.Controls.Add(this.buyEur);
            this.Controls.Add(this.sellUsd);
            this.Controls.Add(this.buyUSD);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CurrencyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.CurrencyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exgStripes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label sellRub;
        private System.Windows.Forms.Label buyRub;
        private System.Windows.Forms.Label sellEur;
        private System.Windows.Forms.Label buyEur;
        private System.Windows.Forms.Label sellUsd;
        private System.Windows.Forms.Label buyUSD;
        private System.Windows.Forms.Label text;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox SellUAH;
        private System.Windows.Forms.PictureBox exgStripes;
        private System.Windows.Forms.ComboBox currencyToConvert;
        private System.Windows.Forms.TextBox BuyCurr;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CurrencyToConvert1;
        private System.Windows.Forms.TextBox SellCur;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox BuyUAH;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox Closebtn;
        private System.Windows.Forms.Label today;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
    }
}