﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;

namespace Bank
{
    public partial class RegistrationForm : Form
    {
        private void eyepict_Click(object sender, EventArgs e)
        {
            if (vis % 2 == 0)
            {
                eyepict.Load(@"../../../Pictures\openeye.png");
                uPassword.UseSystemPasswordChar = false;
                vis++;
            }

            else if (vis % 2 == 1)
            {
                eyepict.Load(@"../../../Pictures\hiddeneye.png");
                uPassword.UseSystemPasswordChar = true;
                vis--;
            }
        }
        public RegistrationForm()
        {
            InitializeComponent();
        }
        int vis = 2;

        Auth auth = new Auth();

        User newUser = new User();
        User existUser = new User();


        private void RegistrationForm_Load(object sender, EventArgs e)
        {

        }
        private void phonenumber_TextChanged(object sender, EventArgs e)
        {
            if (phonenumber.Text == "" || phonenumber.Text == "3" || phonenumber.Text == "38")
            {

                phonenumber.SelectionStart = phonenumber.TextLength;
                phonenumber.ScrollToCaret();
            }
            if (phonenumber.Text == "380" || !Regex.IsMatch(phonenumber.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                PhoneCheck.Load(@"../../../Pictures\incorrect.png");
            }
            else if (phonenumber.Text != "" || Regex.IsMatch(phonenumber.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                PhoneCheck.Load(@"../../../Pictures\correct.png");
            }
        }

        private void cardNumber_TextChanged(object sender, EventArgs e)
        {
            if (cardNumber.Text == "" || !Regex.IsMatch(cardNumber.Text, @"^\d{16}$", RegexOptions.Compiled))
            {
                CardCheck.Load(@"../../../Pictures\incorrect.png");
            }
            else if (cardNumber.Text != "" || Regex.IsMatch(cardNumber.Text, @"^\d{16}$", RegexOptions.Compiled))
            {
                CardCheck.Load(@"../../../Pictures\correct.png");
            }
        }

        private void uPassword_TextChanged(object sender, EventArgs e)
        {
            if (uPassword.Text == "" || !Regex.IsMatch(uPassword.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled))
            {
                PasswordCheck.Load(@"../../../Pictures\incorrect.png");
            }
            else if (uPassword.Text != "" && Regex.IsMatch(uPassword.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled))
            {
                PasswordCheck.Load(@"../../../Pictures\correct.png");
            }
        }

        private void uEmail_TextChanged(object sender, EventArgs e)
        {
            if (uEmail.Text == "" || !Regex.IsMatch(uEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                EmailCheck.Load(@"../../../Pictures\incorrect.png");
            }
            else if (uEmail.Text != "" || Regex.IsMatch(uEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                EmailCheck.Load(@"../../../Pictures\correct.png");
            }
        }

        private void uSurname_TextChanged(object sender, EventArgs e)
        {
            if (uSurname.Text == "" || !Regex.IsMatch(uSurname.Text, @"^[А-ЯІЄ][а-яіє]+(\-[А-ЯІЄ][а-яіє]+)?$", RegexOptions.Compiled))
            {
                SurnameCheck.Load(@"../../../Pictures\incorrect.png");
            }
            else if (uSurname.Text != "" && Regex.IsMatch(uSurname.Text, @"^[А-ЯІЄ][а-яіє]+(\-[А-ЯІЄ][а-яіє]+)?$", RegexOptions.Compiled))
            {
                SurnameCheck.Load(@"../../../Pictures\correct.png");
            }
        }

        private void uName_TextChanged(object sender, EventArgs e)
        {
            if (uName.Text == "" || !Regex.IsMatch(uName.Text, @"^[А-ЯІЄ][а-яіє]+(\-[А-ЯІЄ][а-яіє]+)?$", RegexOptions.Compiled))
            {
                NameCheck.Load(@"../../../Pictures\incorrect.png");
            }
            else if (uName.Text != "" && Regex.IsMatch(uName.Text, @"^[А-ЯІЄ][а-яіє]+(\-[А-ЯІЄ][а-яіє]+)?$", RegexOptions.Compiled))
            {
                NameCheck.Load(@"../../../Pictures\correct.png");
            }
        }

        private void uPatronym_TextChanged(object sender, EventArgs e)
        {
            if (uPatronym.Text == "" || !Regex.IsMatch(uPatronym.Text, @"^[А-ЯІЄ][а-яіє]+?$", RegexOptions.Compiled))
            {
                PatronymCheck.Load(@"../../../Pictures\incorrect.png");
            }
            else if (uPatronym.Text != "" && Regex.IsMatch(uPatronym.Text, @"^[А-ЯІЄ][а-яіє]+(\-[А-ЯІЄ][а-яіє]+)?$", RegexOptions.Compiled))
            {
                PatronymCheck.Load(@"../../../Pictures\correct.png");
            }
        }
        private void Regbtn_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(uPatronym.Text, @"^[А-ЯІЄ][а-яіє]+?$", RegexOptions.Compiled) && Regex.IsMatch(uName.Text, @"^[А-ЯІЄ][а-яіє]+?$", RegexOptions.Compiled) && Regex.IsMatch(uSurname.Text, @"^[А-ЯІЄ][а-яіє]+?$", RegexOptions.Compiled) && Regex.IsMatch(cardNumber.Text, @"^\d{16}$", RegexOptions.Compiled) && Regex.IsMatch(uEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled) && Regex.IsMatch(uPassword.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && Regex.IsMatch(phonenumber.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                existUser.UserExist(long.Parse(phonenumber.Text));
                existUser.UserExist(cardNumber.Text);
                if (existUser.counter != 0 && existUser.cardCounter != 0)
                {


                    newUser.NewUser(uName.Text, uSurname.Text, uPatronym.Text, cardNumber.Text, uEmail.Text, long.Parse(phonenumber.Text), uPassword.Text);
                    string path = $@"../../../History\history_of_user#" + cardNumber.Text + ".txt";
                    File.Create(path);
                    WinNotify.ShowWinNotify("Новий аккаунт", $"{uName.Text} {uPatronym.Text}, вітаємо із створенням аккаунту!\nВітаємо в Банку!", 7000);
                    Close();
                    Auth auth = new Auth();
                    auth.Show();
                }
                else
                {
                    MessageBox.Show($"{uName.Text}, вибачте, але такий користувач вже існує.", "Такий користувач вже існує", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вибачте, ви не можете створити аккаунт, поки у вас введені невірні дані.", "Помилка створення аккаунту", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
            Auth auth = new Auth();
            auth.Show();
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Supportbtn_Click(object sender, EventArgs e)
        {
            Process.Start("http://t.me/CourseBankBot");
        }

        private void PhoneCheck_Click(object sender, EventArgs e)
        {

        }
    }
}
