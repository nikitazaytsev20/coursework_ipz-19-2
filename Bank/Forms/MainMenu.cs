﻿using Bank.Forms;
using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;
namespace Bank
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }
        public long login;
        User logUser = new User();
        private void MainMenu_Load(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.CardNumber, "Для того щоб скопіювати номер карти натисніть два рази");
            logUser.UserConnection(login);
            user.Text = DateTime.Now.ToShortDateString();
            user.Left = (this.Width - user.Width) / 2;
            userPIB.Text = logUser.userSurname + " " + logUser.userName + " " + logUser.userPatronym;
            UserBalance.Text = logUser.uah.ToString("F2").Replace(",", ".") + " ₴";
            string cardPattern = @"\d{12}";
            Regex regex = new Regex(cardPattern);
            string replaceText = "****";
            CardNumber.Text = regex.Replace(logUser.userCard, replaceText);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Support_Click(object sender, EventArgs e)
        {
            DialogResult askTelegram = MessageBox.Show("Ви бажаєте перейти на інший сайт?", "Служба підтримки", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (askTelegram == DialogResult.Yes)
            {
                Process.Start("http://t.me/CourseBankBot");
            }

        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Donations_Click(object sender, EventArgs e)
        {
            Donations donations = new Donations();
            donations.login = login;
            donations.Show();
            this.Close();
        }

        private void UserBalance_Click(object sender, EventArgs e)
        {

        }

        private void Transfer_Click(object sender, EventArgs e)
        {
            Transactions transactions = new Transactions();
            transactions.login = login;
            transactions.Show();
            this.Close();
        }

        private void UserHistory_Click(object sender, EventArgs e)
        {
            UserHist history = new UserHist();
            this.Opacity = .2;
            history.name = logUser.userName;
            history.surname = logUser.userSurname;
            history.patronym = logUser.userPatronym;
            history.card = logUser.userCard;
            history.ShowDialog();
            this.Opacity = 1;

        }

        private void TradeMoney_Click(object sender, EventArgs e)
        {
            CurrencyForm currencyForm = new CurrencyForm();
            currencyForm.login = login;
            currencyForm.Show();
            this.Close();
        }

        private void UserPhone_Click(object sender, EventArgs e)
        {
            Phone phonepay = new Phone();
            MainMenu mainMenu = new MainMenu();
            this.Close();
            phonepay.login = login;
            phonepay.Show();

        }

        private void Home_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.login = login;
            home.Show();
            this.Close();
        }

        private void InternetPay_Click(object sender, EventArgs e)
        {
            InternetPayments internet = new InternetPayments();
            internet.login = login;
            internet.Show();
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void CardNumber_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText(logUser.userCard);
        }

        private void CardNumber_MouseMove(object sender, MouseEventArgs e)
        {
            
        }
    }
}
