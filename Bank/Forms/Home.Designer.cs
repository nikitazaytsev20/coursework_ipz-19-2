﻿namespace Bank.Forms
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.pay = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.flatPayText = new System.Windows.Forms.Label();
            this.trashText = new System.Windows.Forms.Label();
            this.electricityText = new System.Windows.Forms.Label();
            this.waterText = new System.Windows.Forms.Label();
            this.gazText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.Closebtn = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.gazcount = new System.Windows.Forms.TextBox();
            this.watercount = new System.Windows.Forms.TextBox();
            this.electricitycount = new System.Windows.Forms.TextBox();
            this.trashcount = new System.Windows.Forms.TextBox();
            this.homecount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.heatcount = new System.Windows.Forms.TextBox();
            this.userAdress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.SuspendLayout();
            // 
            // pay
            // 
            this.pay.BackColor = System.Drawing.Color.Transparent;
            this.pay.FlatAppearance.BorderSize = 0;
            this.pay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pay.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pay.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pay.Location = new System.Drawing.Point(118, 599);
            this.pay.Name = "pay";
            this.pay.Size = new System.Drawing.Size(183, 97);
            this.pay.TabIndex = 71;
            this.pay.Text = "Оплатити";
            this.pay.UseVisualStyleBackColor = false;
            this.pay.Click += new System.EventHandler(this.pay_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(325, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 18);
            this.label2.TabIndex = 70;
            this.label2.Text = "Сума";
            // 
            // flatPayText
            // 
            this.flatPayText.AutoSize = true;
            this.flatPayText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.flatPayText.Location = new System.Drawing.Point(89, 479);
            this.flatPayText.Name = "flatPayText";
            this.flatPayText.Size = new System.Drawing.Size(110, 18);
            this.flatPayText.TabIndex = 63;
            this.flatPayText.Text = "Квартплата:";
            // 
            // trashText
            // 
            this.trashText.AutoSize = true;
            this.trashText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.trashText.Location = new System.Drawing.Point(89, 412);
            this.trashText.Name = "trashText";
            this.trashText.Size = new System.Drawing.Size(160, 18);
            this.trashText.TabIndex = 62;
            this.trashText.Text = "Вивезення сміття:";
            // 
            // electricityText
            // 
            this.electricityText.AutoSize = true;
            this.electricityText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.electricityText.Location = new System.Drawing.Point(89, 342);
            this.electricityText.Name = "electricityText";
            this.electricityText.Size = new System.Drawing.Size(173, 18);
            this.electricityText.TabIndex = 61;
            this.electricityText.Text = "За електроенергію:";
            // 
            // waterText
            // 
            this.waterText.AutoSize = true;
            this.waterText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.waterText.Location = new System.Drawing.Point(89, 277);
            this.waterText.Name = "waterText";
            this.waterText.Size = new System.Drawing.Size(80, 18);
            this.waterText.TabIndex = 60;
            this.waterText.Text = "За воду:";
            // 
            // gazText
            // 
            this.gazText.AutoSize = true;
            this.gazText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gazText.Location = new System.Drawing.Point(89, 210);
            this.gazText.Name = "gazText";
            this.gazText.Size = new System.Drawing.Size(68, 18);
            this.gazText.TabIndex = 59;
            this.gazText.Text = "За газ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(37, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 25);
            this.label1.TabIndex = 52;
            this.label1.Text = "Плата за комунальні послуги";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Image = global::Bank.Properties.Resources.House;
            this.pictureBox5.Location = new System.Drawing.Point(33, 461);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 58;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::Bank.Properties.Resources.Trash;
            this.pictureBox4.Location = new System.Drawing.Point(33, 392);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 57;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Bank.Properties.Resources.Electricity;
            this.pictureBox1.Location = new System.Drawing.Point(33, 323);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Bank.Properties.Resources.Water;
            this.pictureBox2.Location = new System.Drawing.Point(33, 257);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 55;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Image = global::Bank.Properties.Resources.gas;
            this.pictureBox6.Location = new System.Drawing.Point(33, 192);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(50, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 54;
            this.pictureBox6.TabStop = false;
            // 
            // Closebtn
            // 
            this.Closebtn.BackColor = System.Drawing.SystemColors.Window;
            this.Closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Closebtn.Image = global::Bank.Properties.Resources.Close;
            this.Closebtn.Location = new System.Drawing.Point(385, 11);
            this.Closebtn.Margin = new System.Windows.Forms.Padding(2);
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.Size = new System.Drawing.Size(40, 40);
            this.Closebtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Closebtn.TabIndex = 51;
            this.Closebtn.TabStop = false;
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::Bank.Properties.Resources.back;
            this.pictureBox3.Location = new System.Drawing.Point(11, 11);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 40);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 50;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox7.Location = new System.Drawing.Point(-3, 726);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(443, 61);
            this.pictureBox7.TabIndex = 75;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox8.Location = new System.Drawing.Point(-3, -57);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(443, 61);
            this.pictureBox8.TabIndex = 74;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox9.Location = new System.Drawing.Point(-3, -5);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(8, 741);
            this.pictureBox9.TabIndex = 73;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox10.Location = new System.Drawing.Point(432, -7);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(8, 741);
            this.pictureBox10.TabIndex = 72;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.Image = global::Bank.Properties.Resources.gas;
            this.pictureBox11.Location = new System.Drawing.Point(33, 527);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(50, 50);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 76;
            this.pictureBox11.TabStop = false;
            // 
            // gazcount
            // 
            this.gazcount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gazcount.Location = new System.Drawing.Point(328, 202);
            this.gazcount.Name = "gazcount";
            this.gazcount.Size = new System.Drawing.Size(56, 31);
            this.gazcount.TabIndex = 77;
            this.gazcount.Text = "0";
            this.gazcount.Click += new System.EventHandler(this.gazcount_Click);
            // 
            // watercount
            // 
            this.watercount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.watercount.Location = new System.Drawing.Point(328, 269);
            this.watercount.Name = "watercount";
            this.watercount.Size = new System.Drawing.Size(56, 31);
            this.watercount.TabIndex = 78;
            this.watercount.Text = "0";
            this.watercount.Click += new System.EventHandler(this.watercount_Click);
            // 
            // electricitycount
            // 
            this.electricitycount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.electricitycount.Location = new System.Drawing.Point(328, 334);
            this.electricitycount.Name = "electricitycount";
            this.electricitycount.Size = new System.Drawing.Size(56, 31);
            this.electricitycount.TabIndex = 79;
            this.electricitycount.Text = "0";
            this.electricitycount.Click += new System.EventHandler(this.electricitycount_Click);
            // 
            // trashcount
            // 
            this.trashcount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.trashcount.Location = new System.Drawing.Point(328, 404);
            this.trashcount.Name = "trashcount";
            this.trashcount.Size = new System.Drawing.Size(56, 31);
            this.trashcount.TabIndex = 80;
            this.trashcount.Text = "0";
            this.trashcount.Click += new System.EventHandler(this.trashcount_Click);
            this.trashcount.TextChanged += new System.EventHandler(this.trashcount_TextChanged);
            // 
            // homecount
            // 
            this.homecount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.homecount.Location = new System.Drawing.Point(328, 471);
            this.homecount.Name = "homecount";
            this.homecount.Size = new System.Drawing.Size(56, 31);
            this.homecount.TabIndex = 81;
            this.homecount.Text = "0";
            this.homecount.Click += new System.EventHandler(this.homecount_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(89, 547);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 18);
            this.label3.TabIndex = 82;
            this.label3.Text = "За опалення:";
            // 
            // heatcount
            // 
            this.heatcount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.heatcount.Location = new System.Drawing.Point(328, 539);
            this.heatcount.Name = "heatcount";
            this.heatcount.Size = new System.Drawing.Size(56, 31);
            this.heatcount.TabIndex = 83;
            this.heatcount.Text = "0";
            this.heatcount.Click += new System.EventHandler(this.heatcount_Click);
            // 
            // userAdress
            // 
            this.userAdress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.userAdress.Location = new System.Drawing.Point(202, 111);
            this.userAdress.Name = "userAdress";
            this.userAdress.Size = new System.Drawing.Size(202, 24);
            this.userAdress.TabIndex = 85;
            this.userAdress.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.userAdress.Leave += new System.EventHandler(this.userAdress_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(26, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 29);
            this.label4.TabIndex = 86;
            this.label4.Text = "Ваша адреса: ";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(436, 731);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.userAdress);
            this.Controls.Add(this.heatcount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.homecount);
            this.Controls.Add(this.trashcount);
            this.Controls.Add(this.electricitycount);
            this.Controls.Add(this.watercount);
            this.Controls.Add(this.gazcount);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flatPayText);
            this.Controls.Add(this.trashText);
            this.Controls.Add(this.electricityText);
            this.Controls.Add(this.waterText);
            this.Controls.Add(this.gazText);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.pictureBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Home_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Closebtn;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button pay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label flatPayText;
        private System.Windows.Forms.Label trashText;
        private System.Windows.Forms.Label electricityText;
        private System.Windows.Forms.Label waterText;
        private System.Windows.Forms.Label gazText;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.TextBox gazcount;
        private System.Windows.Forms.TextBox watercount;
        private System.Windows.Forms.TextBox electricitycount;
        private System.Windows.Forms.TextBox trashcount;
        private System.Windows.Forms.TextBox homecount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox heatcount;
        private System.Windows.Forms.TextBox userAdress;
        private System.Windows.Forms.Label label4;
    }
}