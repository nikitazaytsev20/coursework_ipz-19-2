﻿using System;
using System.Windows.Forms;

namespace Bank.Forms
{
    public partial class Donations : Form
    {
        public Donations()
        {
            InitializeComponent();
        }
        MainMenu mainMenu = new MainMenu();
        public long login;

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Donations_Load(object sender, EventArgs e)
        {
            label1.Text = "Поповнення ігрового\nрахунку";
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Donations donation = new Donations();
            mainMenu.login = login;
            this.Close();
            mainMenu.Show();
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Steam_Click_1(object sender, EventArgs e)
        {
            DonationSteam steam = new DonationSteam();
            steam.login = login;
            this.Close();
            steam.Show();
        }

        private void WarGaming_Click(object sender, EventArgs e)
        {
            DonationsWargaming Wargaming = new DonationsWargaming();
            Wargaming.login = login;
            this.Close();
            Wargaming.Show();
        }

        private void BattleNet_Click(object sender, EventArgs e)
        {
            DonationsBattleNet BattleNet = new DonationsBattleNet();
            BattleNet.login = login;
            this.Close();
            BattleNet.Show();
        }
    }
}
