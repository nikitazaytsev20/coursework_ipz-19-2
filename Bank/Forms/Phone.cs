﻿using HistoryClass;
using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;

namespace Bank.Forms
{
    public partial class Phone : Form
    {
        public Phone()
        {
            InitializeComponent();
        }
        public long login;

        public float amount;

        int clicker = 0;

        int changed = 0;

        int errCount = 0;

        int errPhn = 0;

        MainMenu mainMenu = new MainMenu();

        UserTransactions user = new UserTransactions();
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Phone_Load(object sender, EventArgs e)
        {
            phonenumbertext.Text = login.ToString();
            user.UserConnection(login);

        }

        private void Amount_TextChanged(object sender, EventArgs e)
        {
            Amount.MaxLength = 8;
        }

        private void Amount_Leave(object sender, EventArgs e)
        {
            changed = 0;
            if (Amount.Text == "")
            {
                changed++;
                Amount.Text = "Сума";
                Amount.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(Amount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCount <= 4)
            {
                errCount++;
                Amount.Clear();
                Amount.Focus();
                MessageBox.Show("Введена сума не відповідає заданому формату! Перевірте, чи введіть ще раз.");
            }
            if (errCount > 4)
            {
                Amount.Clear();
                Amount.Focus();
                Amount.ForeColor = Color.Black;
                WinNotify.ShowWinNotify("Помилка вводу суми", $"{user.userName}, Ви вводите невірний формат суми.\nПриклад правильного вводу формату: 12.34 або 15", 10000);
            }
        }

        private void Amount_Click(object sender, EventArgs e)
        {
            Amount.Clear();
        }

        private void Phone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pay.PerformClick();
            }
            if (e.KeyCode == Keys.Down)
            {
                Amount.Focus();
            }
            if (e.KeyCode == Keys.Up)
            {
                phonenumbertext.Focus();
            }
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult dialog = MessageBox.Show("Ви впевнені, що хочете повернутися в головне меню?", "Вихід із меню", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    this.Close();
                    mainMenu.login = login;
                    mainMenu.Show();
                }
            }
        }

        private void phonenumbertext_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(phonenumbertext.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}", RegexOptions.Compiled) && errPhn <= 3)
            {
                errPhn++;
                MessageBox.Show("Невірно введено номер! Перевірте ще раз!");
            }
            if (errPhn > 3)
            {
                WinNotify.ShowWinNotify("Помилка вводу номеру", $"{user.userName}, Ви вводите невірний формат номеру телефона.\nПриклади правильного формату: 380966020340 / 380968265868", 10000);
            }
        }

        private void pay_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(Amount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(phonenumbertext.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}", RegexOptions.Compiled))
            {
                try
                {
                    string phoneNum = phonenumbertext.Text;
                    amount = float.Parse(Amount.Text.Replace(".", ","));

                    if (user.uah >= amount)
                    {



                        user.uah -= amount;
                        user.UserTransfer(login, user.uah);
                        Close();

                        History.AddToHistory(user.userCard, $"{DateTime.Now.ToShortDateString()} | {amount} – Поповнення мобільного телефону {phoneNum}");
                        WinNotify.ShowWinNotify("Поповнення рахунку телефона", $"Мобільний рахунок {phoneNum} був успішно поповнений на суму в розмірі {amount}₴\nВаш поточний баланс: {user.uah:0.##}₴", 5000);
                        mainMenu.login = login;
                        mainMenu.Show();
                    }
                    else
                    {
                        MessageBox.Show($"У вас недостатньо коштів для поповнення рахунку. Введіть суму яка більша, або дорівнює - {user.uah}.", "Недостатньо коштів", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Amount.Focus();
                        this.Opacity = 1;
                        return;
                    }
                }
                catch (FormatException fe)
                {
                    MessageBox.Show(fe.Message);
                }
            }
            else
            {
                MessageBox.Show("Вибачте, ви ввели невірні дані.", "Помилка формату", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Phone phone = new Phone();
            mainMenu.login = login;
            this.Close();
            mainMenu.Show();
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
