﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;


namespace Bank
{
    public partial class Auth : Form
    {
        public Auth()
        {
            InitializeComponent();
        }
        public long tryLog;
        int vis = 2;
        User user = new User();

        private void Auth_Load(object sender, EventArgs e)
        {


        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void PhoneNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void Registr_Click(object sender, EventArgs e)
        {
            Hide();
            RegistrationForm registrationForm = new RegistrationForm();
            registrationForm.Show();
        }

        private void Supportbtn_Click(object sender, EventArgs e)
        {
            Process.Start("http://t.me/CourseBankBot");
        }

        private void LogIn_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(PhoneNumber.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                LogIn.ForeColor = Color.FromArgb(127, 10, 10);
                LogIn.BackColor = Color.FromArgb(240, 240, 240);
                tryLog = long.Parse(PhoneNumber.Text);
                string tryPass = PasswordText.Text;
                this.Visible = false;
                try
                {
                    user.UserAuthing(tryLog, tryPass);
                    if (user.counter != 0)
                    {
                        MainMenu mainMenu = new MainMenu();
                        this.Hide();
                        mainMenu.login = tryLog;
                        mainMenu.Show();
                    }

                    else if (user.counter == 0)
                    {
                        this.Visible = true;
                        MessageBox.Show("Ви ввели невірні дані! Перевірте та введіть ще раз!", "Помилка аутентифікації", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                catch (FormatException)
                {
                    LogIn.ForeColor = Color.FromArgb(255, 0, 0);
                    MessageBox.Show("Ви ввели невірні дані! Перевірте та введіть ще раз!", "Помилка аутентифікації", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Ви ввели некорректні значення!", "Помилка аутентифікації", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void eyepict_Click(object sender, EventArgs e)
        {

            if (vis % 2 == 0)
            {
                eyepict.Load(@"../../../Pictures\openeye.png");
                PasswordText.UseSystemPasswordChar = false;
                vis++;
            }

            else if (vis % 2 == 1)
            {
                eyepict.Load(@"../../../Pictures\hiddeneye.png");
                PasswordText.UseSystemPasswordChar = true;
                vis--;
            }
        }
    }
}
