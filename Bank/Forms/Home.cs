﻿using HistoryClass;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;
namespace Bank.Forms
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }
        public long login;

        float amount;

        Random rnd = new Random();

        float gaz, water, electricity, trash, flatPay, heatpay;

        UserTransactions user = new UserTransactions();

        MainMenu mainMenu = new MainMenu();

        int check = 0;
        string adress;
        int comite = 1;

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            mainMenu.login = login;
            this.Close();
            mainMenu.Show();
        }

        private void gazcount_Click(object sender, EventArgs e)
        {
            gazcount.Clear();
        }

        private void watercount_Click(object sender, EventArgs e)
        {
            watercount.Clear();
        }

        private void electricitycount_Click(object sender, EventArgs e)
        {
            electricitycount.Clear();
        }

        private void trashcount_TextChanged(object sender, EventArgs e)
        {

        }

        private void trashcount_Click(object sender, EventArgs e)
        {
            trashcount.Clear();
        }

        private void homecount_Click(object sender, EventArgs e)
        {
            homecount.Clear();
        }

        private void userAdress_Leave(object sender, EventArgs e)
        {
            if (Regex.IsMatch(userAdress.Text, @"^(.+)\s+(\S+?)(-(\d+))?$"))
            {

            }else
            {
                MessageBox.Show("Невірно вказаний адрес, введіть ще раз", "Помилка вводу адресу", MessageBoxButtons.OK, MessageBoxIcon.Error);
                userAdress.Clear();
            }
        }

        private void heatcount_Click(object sender, EventArgs e)
        {
            heatcount.Clear();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            
            user.UserConnection(login);

        }

        private void pay_Click(object sender, EventArgs e)
        {

           
            if (Regex.IsMatch(userAdress.Text, @"^(.+)\s+(\S+?)(-(\d+))?$") && Regex.IsMatch(gazcount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(watercount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(electricitycount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(trashcount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(homecount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(heatcount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled))
            {
                try
                {
                    gaz = float.Parse(gazcount.Text.Replace(".", ","));
                    water = float.Parse(watercount.Text.Replace(".", ","));
                    electricity = float.Parse(electricitycount.Text.Replace(".", ","));
                    trash = float.Parse(trashcount.Text.Replace(".", ","));
                    flatPay = float.Parse(homecount.Text.Replace(".", ","));
                    heatpay = float.Parse(heatcount.Text.Replace(".", ","));
                    adress = userAdress.Text;
                    float commite = amount * comite / 100;
                    amount = gaz + water + electricity + trash + flatPay + heatpay + commite;

                    if (user.uah >= amount)
                    {

                        user.uah -= amount;
                        user.UserTransfer(login, user.uah);
                        Close();
                        History.AddToHistory(user.userCard, $"{DateTime.Now.ToShortDateString()} | {amount} – Оплата комунальних послуг {adress}");
                        WinNotify.ShowWinNotify("Оплата комунальних послуг", $"Ви успішно оплатили комунальні послуги {adress} за {amount}₴\nВаш поточний баланс: {user.uah:0.##}₴", 5000);
                        mainMenu.login = login;
                        mainMenu.Show();

                    }
                    else
                    {
                        MessageBox.Show($"{user.userName}, у вас недостатньо коштів щоб оплатити ці послуги.", "Недостатньо коштів", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        amount = 0;
                        return;
                    }
                }
                catch (FormatException fe)
                {
                    MessageBox.Show(fe.Message);
                }
            }
            else
            {
                MessageBox.Show("Вибачте, ви ввели невірні дані.", "Помилка формату", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }        
    }
}
