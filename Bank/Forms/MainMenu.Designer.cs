﻿namespace Bank
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.UserHistory = new System.Windows.Forms.PictureBox();
            this.TradeMoney = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Support = new System.Windows.Forms.PictureBox();
            this.Transfer = new System.Windows.Forms.PictureBox();
            this.UserPhone = new System.Windows.Forms.PictureBox();
            this.Donations = new System.Windows.Forms.PictureBox();
            this.InternetPay = new System.Windows.Forms.PictureBox();
            this.Home = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CardNumber = new System.Windows.Forms.Label();
            this.user = new System.Windows.Forms.Label();
            this.UserBalance = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.userPIB = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.UserHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TradeMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Support)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Transfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Donations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternetPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // UserHistory
            // 
            this.UserHistory.BackColor = System.Drawing.SystemColors.Window;
            this.UserHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UserHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UserHistory.Image = global::Bank.Properties.Resources.History;
            this.UserHistory.Location = new System.Drawing.Point(24, 14);
            this.UserHistory.Margin = new System.Windows.Forms.Padding(2);
            this.UserHistory.Name = "UserHistory";
            this.UserHistory.Size = new System.Drawing.Size(96, 96);
            this.UserHistory.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UserHistory.TabIndex = 0;
            this.UserHistory.TabStop = false;
            this.UserHistory.Click += new System.EventHandler(this.UserHistory_Click);
            // 
            // TradeMoney
            // 
            this.TradeMoney.BackColor = System.Drawing.SystemColors.Window;
            this.TradeMoney.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TradeMoney.Image = global::Bank.Properties.Resources.USD_RUB_EUR;
            this.TradeMoney.Location = new System.Drawing.Point(336, 14);
            this.TradeMoney.Margin = new System.Windows.Forms.Padding(2);
            this.TradeMoney.Name = "TradeMoney";
            this.TradeMoney.Size = new System.Drawing.Size(96, 96);
            this.TradeMoney.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.TradeMoney.TabIndex = 1;
            this.TradeMoney.TabStop = false;
            this.TradeMoney.Click += new System.EventHandler(this.TradeMoney_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Bank.Properties.Resources.Close;
            this.pictureBox1.Location = new System.Drawing.Point(10, 724);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Support
            // 
            this.Support.BackColor = System.Drawing.SystemColors.Window;
            this.Support.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Support.Image = global::Bank.Properties.Resources.Support;
            this.Support.Location = new System.Drawing.Point(402, 724);
            this.Support.Margin = new System.Windows.Forms.Padding(2);
            this.Support.Name = "Support";
            this.Support.Size = new System.Drawing.Size(40, 40);
            this.Support.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Support.TabIndex = 3;
            this.Support.TabStop = false;
            this.Support.Click += new System.EventHandler(this.Support_Click);
            // 
            // Transfer
            // 
            this.Transfer.BackColor = System.Drawing.SystemColors.Window;
            this.Transfer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Transfer.Image = global::Bank.Properties.Resources.transfer11;
            this.Transfer.Location = new System.Drawing.Point(24, 373);
            this.Transfer.Margin = new System.Windows.Forms.Padding(0);
            this.Transfer.Name = "Transfer";
            this.Transfer.Size = new System.Drawing.Size(403, 77);
            this.Transfer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Transfer.TabIndex = 5;
            this.Transfer.TabStop = false;
            this.Transfer.Click += new System.EventHandler(this.Transfer_Click);
            // 
            // UserPhone
            // 
            this.UserPhone.BackColor = System.Drawing.SystemColors.Window;
            this.UserPhone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UserPhone.Image = global::Bank.Properties.Resources.Phone2_0;
            this.UserPhone.Location = new System.Drawing.Point(223, 488);
            this.UserPhone.Margin = new System.Windows.Forms.Padding(2);
            this.UserPhone.Name = "UserPhone";
            this.UserPhone.Size = new System.Drawing.Size(218, 96);
            this.UserPhone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UserPhone.TabIndex = 6;
            this.UserPhone.TabStop = false;
            this.UserPhone.Click += new System.EventHandler(this.UserPhone_Click);
            // 
            // Donations
            // 
            this.Donations.BackColor = System.Drawing.SystemColors.Window;
            this.Donations.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Donations.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Donations.Image = global::Bank.Properties.Resources.Games2_0;
            this.Donations.Location = new System.Drawing.Point(11, 488);
            this.Donations.Margin = new System.Windows.Forms.Padding(2);
            this.Donations.Name = "Donations";
            this.Donations.Size = new System.Drawing.Size(208, 96);
            this.Donations.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Donations.TabIndex = 7;
            this.Donations.TabStop = false;
            this.Donations.Click += new System.EventHandler(this.Donations_Click);
            // 
            // InternetPay
            // 
            this.InternetPay.BackColor = System.Drawing.SystemColors.Window;
            this.InternetPay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.InternetPay.Image = global::Bank.Properties.Resources.InternetPayments;
            this.InternetPay.Location = new System.Drawing.Point(7, 605);
            this.InternetPay.Margin = new System.Windows.Forms.Padding(2);
            this.InternetPay.Name = "InternetPay";
            this.InternetPay.Size = new System.Drawing.Size(212, 96);
            this.InternetPay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.InternetPay.TabIndex = 8;
            this.InternetPay.TabStop = false;
            this.InternetPay.Click += new System.EventHandler(this.InternetPay_Click);
            // 
            // Home
            // 
            this.Home.BackColor = System.Drawing.SystemColors.Window;
            this.Home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Home.Image = global::Bank.Properties.Resources.Home2_0;
            this.Home.Location = new System.Drawing.Point(223, 605);
            this.Home.Margin = new System.Windows.Forms.Padding(2);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(218, 96);
            this.Home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Home.TabIndex = 9;
            this.Home.TabStop = false;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Window;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(11, 292);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 29);
            this.label1.TabIndex = 10;
            this.label1.Text = "UAH";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // CardNumber
            // 
            this.CardNumber.AutoSize = true;
            this.CardNumber.BackColor = System.Drawing.SystemColors.Window;
            this.CardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CardNumber.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.CardNumber.Location = new System.Drawing.Point(80, 296);
            this.CardNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CardNumber.Name = "CardNumber";
            this.CardNumber.Size = new System.Drawing.Size(108, 29);
            this.CardNumber.TabIndex = 14;
            this.CardNumber.Text = "**** ****";
            this.CardNumber.DoubleClick += new System.EventHandler(this.CardNumber_DoubleClick);
            this.CardNumber.MouseMove += new System.Windows.Forms.MouseEventHandler(this.CardNumber_MouseMove);
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.BackColor = System.Drawing.SystemColors.Window;
            this.user.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.user.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.user.Location = new System.Drawing.Point(136, 125);
            this.user.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(0, 24);
            this.user.TabIndex = 15;
            // 
            // UserBalance
            // 
            this.UserBalance.AutoSize = true;
            this.UserBalance.BackColor = System.Drawing.SystemColors.Window;
            this.UserBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UserBalance.Location = new System.Drawing.Point(11, 325);
            this.UserBalance.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.UserBalance.Name = "UserBalance";
            this.UserBalance.Size = new System.Drawing.Size(132, 29);
            this.UserBalance.TabIndex = 16;
            this.UserBalance.Text = "999999,99";
            this.UserBalance.Click += new System.EventHandler(this.UserBalance_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox2.Location = new System.Drawing.Point(-12, 249);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(467, 201);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // userPIB
            // 
            this.userPIB.AutoSize = true;
            this.userPIB.BackColor = System.Drawing.SystemColors.Window;
            this.userPIB.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.userPIB.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.userPIB.Location = new System.Drawing.Point(17, 188);
            this.userPIB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.userPIB.Name = "userPIB";
            this.userPIB.Size = new System.Drawing.Size(0, 37);
            this.userPIB.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Window;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(135, 249);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 25);
            this.label2.TabIndex = 19;
            this.label2.Text = "Ваш рахунок:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox8.Location = new System.Drawing.Point(-5, -59);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(474, 63);
            this.pictureBox8.TabIndex = 42;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox7.Location = new System.Drawing.Point(447, -6);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 784);
            this.pictureBox7.TabIndex = 41;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox6.Location = new System.Drawing.Point(-5, 765);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(474, 63);
            this.pictureBox6.TabIndex = 40;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox5.Location = new System.Drawing.Point(-5, -6);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(10, 784);
            this.pictureBox5.TabIndex = 39;
            this.pictureBox5.TabStop = false;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Bank.Properties.Resources.BackgroundWhite;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(452, 770);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.userPIB);
            this.Controls.Add(this.UserBalance);
            this.Controls.Add(this.user);
            this.Controls.Add(this.CardNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Home);
            this.Controls.Add(this.InternetPay);
            this.Controls.Add(this.Donations);
            this.Controls.Add(this.UserPhone);
            this.Controls.Add(this.Transfer);
            this.Controls.Add(this.Support);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.TradeMoney);
            this.Controls.Add(this.UserHistory);
            this.Controls.Add(this.pictureBox2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainMenu";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UserHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TradeMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Support)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Transfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Donations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternetPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox UserHistory;
        private System.Windows.Forms.PictureBox TradeMoney;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox Support;
        private System.Windows.Forms.PictureBox Transfer;
        private System.Windows.Forms.PictureBox UserPhone;
        private System.Windows.Forms.PictureBox Donations;
        private System.Windows.Forms.PictureBox InternetPay;
        private System.Windows.Forms.PictureBox Home;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label CardNumber;
        private System.Windows.Forms.Label user;
        private System.Windows.Forms.Label UserBalance;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label userPIB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}