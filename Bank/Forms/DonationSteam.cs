﻿using HistoryClass;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;

namespace Bank.Forms
{
    public partial class DonationSteam : Form
    {
        public DonationSteam()
        {
            InitializeComponent();
        }
        public long login;
        Donations donations = new Donations();
        UserTransactions user = new UserTransactions();
        MainMenu mainMenu = new MainMenu();

        int idChanged = 0;

        int amountChanged = 0;

        int errCountAmount = 0;


        float amount = 0;
        private void DonationSteam_Load(object sender, EventArgs e)
        {
            user.UserConnection(login);
            label1.Text = "Поповнення рахунку\nSteam";
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            DonationsBattleNet donationSteam = new DonationsBattleNet();
            donations.login = login;
            this.Close();
            donations.Show();
        }

        private void payWG_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(Amount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(Id.Text, @"^[\d\w_.]+$", RegexOptions.Compiled))
            {
                try
                {
                    string id = Id.Text;
                    amount = float.Parse(Amount.Text.Replace(".", ","));

                    if (user.uah >= amount)
                    {

                        user.uah -= amount;
                        user.UserTransfer(login, user.uah);
                        History.AddToHistory(user.userCard, $"{DateTime.Now.ToShortDateString()} | {amount} – Поповнення ігрового рахунку Steam {id}");
                        WinNotify.ShowWinNotify("Поповнення рахунку Steam", $"Ви успішнo поповнили рахунок Steam в розмірі {amount}₴\nВаш поточний баланс: {user.uah:0.##}₴", 5000);
                        mainMenu.login = login;
                        mainMenu.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show($"{user.userName},у вас недостатньо коштів для цієї послуги.", "Недостатньо коштів", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        amount = 0;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Введені невірні дані. Перевірте дані та введіть ще раз.", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void Amount_TextChanged(object sender, EventArgs e)
        {
            amountChanged = 0;

            if (!Regex.IsMatch(Amount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCountAmount <= 4)
            {
                errCountAmount++;
                Amount.Clear();
                Amount.Focus();
                MessageBox.Show("Введена сума не задовільняє формат! Перевірте та введіть ще раз.");
            }
            if (errCountAmount > 4)
            {
                Amount.Clear();
                Amount.Focus();
                WinNotify.ShowWinNotify("Помилка вводу суми", $"{user.userName}, Ви вводите неправильний формат суми.\nПриклад правильного вводу суми: 12.34 или 15", 10000);
            }
        }

        private void Id_TextChanged(object sender, EventArgs e)
        {

        }

        private void Id_Click(object sender, EventArgs e)
        {
            Id.Clear();
        }

        private void Amount_Click(object sender, EventArgs e)
        {
            Amount.Clear();
        }
    }
}

