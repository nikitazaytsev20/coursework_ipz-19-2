﻿using HistoryClass;
using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UserClass;

namespace Bank.Forms
{

    public partial class Transactions : Form
    {
        UserTransactions userTransactions = new UserTransactions();

        UserTransactions recipientTransaction = new UserTransactions();
        public long login;

        string recipientCard;

        public float amountForUser;

        float amountForRecipient;

        MainMenu mainMenu = new MainMenu();

        int recipientClicked = 0;

        int recipientChanged = 0;

        int sendingAmountClicked = 0;

        int sendingAmountChanged = 0;

        int errCountRec = 0;

        int errCountAmount = 0;

        int percent = 1;
        public Transactions()
        {
            InitializeComponent();
        }

        private void Transactions_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            sendMoney.Left = (this.Width - sendMoney.Width) / 2;
            userTransactions.SenderLoading(login);
        }

        private void sendMoney_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(recipient.Text, @"^[0-9]{15}", RegexOptions.Compiled) && Regex.IsMatch(sendingAmount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled))
            {
                try
                {
                    amountForUser = float.Parse(sendingAmount.Text.Replace('.', ','));
                    float commite = amountForUser * percent / 100;
                    amountForRecipient = amountForUser + commite;

                    if (userTransactions.uah >= amountForUser)
                    {
                        this.Visible = true;
                        try
                        {
                            try
                            {
                                recipientCard = recipient.Text;
                                recipientTransaction.RecipientLoading(recipientCard);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message + "_____1");
                                return;
                            }
                            if (recipientTransaction.operationSuccsess == 0 || recipientCard == userTransactions.userCard)
                            {
                                recipient.Focus();
                                MessageBox.Show("Ви не можене переказати кошти на цей номер.\nПеревірте вірність введених даних", "Помилка переказу коштів", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else if (recipientTransaction.operationSuccsess != 0 && recipientCard != userTransactions.userCard)
                            {

                                userTransactions.uah -= amountForUser;
                                recipientTransaction.recipientBalance += amountForRecipient;
                                this.Opacity = 1;

                                try
                                {
                                    userTransactions.UserTransfer(login, userTransactions.uah);
                                    recipientTransaction.RecipientTransfer(recipientCard, recipientTransaction.recipientBalance);
                                    this.Close();


                                    History.AddToHistory(userTransactions.userCard, $"{DateTime.Now.ToShortDateString()} | {amountForUser + commite} – Переказ коштів для для {recipientTransaction.recipientName} {recipientTransaction.recipientPatronymic} {recipientTransaction.recipientSurname}");
                                    History.AddToHistory(recipientCard, $"{DateTime.Now.ToShortDateString()} | {amountForRecipient} – Переказ коштів від {userTransactions.userName} {userTransactions.userPatronym} {userTransactions.userSurname}");
                                    WinNotify.ShowWinNotify("Переказ коштів", $"Сума в розмірі {amountForRecipient + commite}₴ були успішно переказані {recipientTransaction.recipientSurname} {recipientTransaction.recipientName} {recipientTransaction.recipientPatronymic}\nВаш поточний баланс: {userTransactions.uah:0.##}₴", 5000);
                                    mainMenu.login = login;
                                    mainMenu.Show();

                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message + "_____2");
                                    return;
                                }
                                this.Opacity = 1;

                            }
                        }
                        catch (FormatException fe)
                        {
                            MessageBox.Show(fe.Message + "_____3");
                        }
                    }
                    else
                    {
                        this.Visible = true;
                        MessageBox.Show($"У вас недостатньо коштів для переказу\nВведіть суму, меншу або рівну - {userTransactions.uah}", "Помилка переводу", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Ви ввели некоректну суму! Введіть заново!", "Помилка вводу", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    sendingAmount.Clear();
                    return;
                }
            }
            else
            {
                MessageBox.Show("Ви ввели невірні дані.", "Помилка формату", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Transactions moneySending = new Transactions();
            mainMenu.login = login;
            this.Close();
            mainMenu.Show();
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void recipient_TextChanged(object sender, EventArgs e)
        {
            recipient.MaxLength = 16;
        }

        private void sendingAmount_TextChanged(object sender, EventArgs e)
        {
            sendingAmount.MaxLength = 8;
        }

        private void recipient_Leave(object sender, EventArgs e)
        {
            recipientChanged = 0;
            if (!Regex.IsMatch(recipient.Text, @"^[0-9]{15}", RegexOptions.Compiled) && errCountRec <= 3)
            {
                errCountRec++;
                MessageBox.Show("Невірно вказаний номер карти! Перевірте ще раз!");
                recipient.ForeColor = Color.Black;
                return;
            }

            else if (errCountRec > 3)
            {
                WinNotify.ShowWinNotify("Помилка вводу карти", $"{userTransactions.userName}, Ви вводите невірний формат карти.\nПриклад правильного номеру карти: 1234567812345678", 10000);
            }
            if (recipient.Text == "")
            {
                recipientChanged++;
                recipient.Text = "Карта отримувача";
                recipient.ForeColor = Color.Gray;
            }
        }

        private void sendingAmount_Leave(object sender, EventArgs e)
        {
            sendingAmountChanged = 0;
            if (!Regex.IsMatch(sendingAmount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCountAmount <= 4)
            {
                errCountAmount++;
                sendingAmount.Clear();
                sendingAmount.Focus();
                MessageBox.Show("Введена сума не відповідає заданному формату! Перевірте, або введіть ще раз.");
                sendingAmount.ForeColor = Color.Black;
                return;
            }

            else if (errCountAmount > 3)
            {
                sendingAmount.Clear();
                sendingAmount.Focus();
                WinNotify.ShowWinNotify("Помилка вводу суми", $"{userTransactions.userName}, Ви вводите невірний формат суми.\nПриклад правильного вводу суми: 12.34 или 15", 10000);
            }

            if (sendingAmount.Text == null)
            {
                sendingAmountChanged++;
                sendingAmount.Text = "Сума";
                sendingAmount.ForeColor = Color.Gray;
            }
        }

        private void recipient_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                sendingAmount.Focus();
                if (sendingAmount != null && sendingAmountClicked == 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.Black;
                    sendingAmountClicked++;
                }

                else if (sendingAmount.Text != null && sendingAmountChanged != 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.Black;
                }
            }
        }

        private void sendingAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                recipient.Focus();
                if (recipient.Text != null && recipientClicked == 0)
                {
                    recipient.Clear();
                    recipient.ForeColor = Color.Black;
                    recipientClicked++;
                }

                else if (recipient.Text != null && recipientChanged != 0)
                {
                    recipient.Clear();
                    recipient.ForeColor = Color.Black;
                }
            }
        }

        private void sendMoney_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sendMoney.PerformClick();
            }
        }

        private void recipient_Click(object sender, EventArgs e)
        {
            recipient.Clear();
        }

        private void sendingAmount_Click(object sender, EventArgs e)
        {
            sendingAmount.Clear();
        }
    }
}
