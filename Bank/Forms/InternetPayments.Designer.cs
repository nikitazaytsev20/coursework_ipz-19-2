﻿namespace Bank.Forms
{
    partial class InternetPayments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InternetPayments));
            this.label1 = new System.Windows.Forms.Label();
            this.PayPal = new System.Windows.Forms.PictureBox();
            this.Qiwi = new System.Windows.Forms.PictureBox();
            this.Closebtn = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.QiwiPanel = new System.Windows.Forms.Panel();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.payQiwi = new System.Windows.Forms.Button();
            this.AmountQiwi = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.IdQiwi = new System.Windows.Forms.TextBox();
            this.QiwiClose = new System.Windows.Forms.PictureBox();
            this.QiwiBack = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PayPalPanel = new System.Windows.Forms.Panel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.PayPalPay = new System.Windows.Forms.Button();
            this.amountPayPal = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.IdPayPal = new System.Windows.Forms.TextBox();
            this.PayPalClose = new System.Windows.Forms.PictureBox();
            this.PayPalBack = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PayPal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qiwi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.QiwiPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QiwiClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QiwiBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PayPalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayPalClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayPalBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(421, 38);
            this.label1.TabIndex = 52;
            this.label1.Text = "Поповнення гаманців";
            // 
            // PayPal
            // 
            this.PayPal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PayPal.Image = global::Bank.Properties.Resources.paypal;
            this.PayPal.Location = new System.Drawing.Point(134, 432);
            this.PayPal.Name = "PayPal";
            this.PayPal.Size = new System.Drawing.Size(175, 175);
            this.PayPal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PayPal.TabIndex = 55;
            this.PayPal.TabStop = false;
            this.PayPal.Click += new System.EventHandler(this.PayPal_Click);
            // 
            // Qiwi
            // 
            this.Qiwi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Qiwi.Image = global::Bank.Properties.Resources.qiwi1;
            this.Qiwi.Location = new System.Drawing.Point(134, 198);
            this.Qiwi.Name = "Qiwi";
            this.Qiwi.Size = new System.Drawing.Size(175, 175);
            this.Qiwi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Qiwi.TabIndex = 54;
            this.Qiwi.TabStop = false;
            this.Qiwi.Click += new System.EventHandler(this.Qiwi_Click);
            // 
            // Closebtn
            // 
            this.Closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Closebtn.Image = global::Bank.Properties.Resources.Close;
            this.Closebtn.Location = new System.Drawing.Point(385, 11);
            this.Closebtn.Margin = new System.Windows.Forms.Padding(2);
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.Size = new System.Drawing.Size(40, 40);
            this.Closebtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Closebtn.TabIndex = 53;
            this.Closebtn.TabStop = false;
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::Bank.Properties.Resources.back;
            this.pictureBox3.Location = new System.Drawing.Point(11, 11);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 40);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 51;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(194, 376);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 24);
            this.label2.TabIndex = 56;
            this.label2.Text = "Qiwi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(184, 610);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 24);
            this.label3.TabIndex = 57;
            this.label3.Text = "PayPal";
            // 
            // QiwiPanel
            // 
            this.QiwiPanel.Controls.Add(this.pictureBox19);
            this.QiwiPanel.Controls.Add(this.pictureBox18);
            this.QiwiPanel.Controls.Add(this.pictureBox17);
            this.QiwiPanel.Controls.Add(this.pictureBox16);
            this.QiwiPanel.Controls.Add(this.payQiwi);
            this.QiwiPanel.Controls.Add(this.AmountQiwi);
            this.QiwiPanel.Controls.Add(this.pictureBox2);
            this.QiwiPanel.Controls.Add(this.pictureBox4);
            this.QiwiPanel.Controls.Add(this.IdQiwi);
            this.QiwiPanel.Controls.Add(this.QiwiClose);
            this.QiwiPanel.Controls.Add(this.QiwiBack);
            this.QiwiPanel.Controls.Add(this.label4);
            this.QiwiPanel.Controls.Add(this.pictureBox1);
            this.QiwiPanel.Location = new System.Drawing.Point(-1, 1);
            this.QiwiPanel.Name = "QiwiPanel";
            this.QiwiPanel.Size = new System.Drawing.Size(446, 740);
            this.QiwiPanel.TabIndex = 58;
            this.QiwiPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.QiwiPanel_Paint);
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox19.Location = new System.Drawing.Point(-159, -9);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(164, 759);
            this.pictureBox19.TabIndex = 215;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox18.Location = new System.Drawing.Point(438, 3);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(756, 759);
            this.pictureBox18.TabIndex = 214;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox17.Location = new System.Drawing.Point(-168, 731);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(756, 291);
            this.pictureBox17.TabIndex = 213;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox16.Location = new System.Drawing.Point(-255, -60);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(258, 899);
            this.pictureBox16.TabIndex = 212;
            this.pictureBox16.TabStop = false;
            // 
            // payQiwi
            // 
            this.payQiwi.FlatAppearance.BorderSize = 0;
            this.payQiwi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payQiwi.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payQiwi.ForeColor = System.Drawing.Color.Gray;
            this.payQiwi.Location = new System.Drawing.Point(92, 517);
            this.payQiwi.Name = "payQiwi";
            this.payQiwi.Size = new System.Drawing.Size(266, 127);
            this.payQiwi.TabIndex = 194;
            this.payQiwi.Text = "Поповнити";
            this.payQiwi.UseVisualStyleBackColor = true;
            this.payQiwi.Click += new System.EventHandler(this.payQiwi_Click);
            // 
            // AmountQiwi
            // 
            this.AmountQiwi.BackColor = System.Drawing.SystemColors.Window;
            this.AmountQiwi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AmountQiwi.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AmountQiwi.ForeColor = System.Drawing.Color.Gray;
            this.AmountQiwi.Location = new System.Drawing.Point(145, 423);
            this.AmountQiwi.Name = "AmountQiwi";
            this.AmountQiwi.Size = new System.Drawing.Size(147, 31);
            this.AmountQiwi.TabIndex = 193;
            this.AmountQiwi.TabStop = false;
            this.AmountQiwi.Text = "Сума";
            this.AmountQiwi.Click += new System.EventHandler(this.AmountQiwi_Click);
            this.AmountQiwi.TextChanged += new System.EventHandler(this.AmountQiwi_TextChanged);
            this.AmountQiwi.Leave += new System.EventHandler(this.AmountQiwi_Leave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pictureBox2.Location = new System.Drawing.Point(145, 460);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(147, 2);
            this.pictureBox2.TabIndex = 192;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pictureBox4.Location = new System.Drawing.Point(125, 370);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(183, 2);
            this.pictureBox4.TabIndex = 191;
            this.pictureBox4.TabStop = false;
            // 
            // IdQiwi
            // 
            this.IdQiwi.BackColor = System.Drawing.SystemColors.Window;
            this.IdQiwi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IdQiwi.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.IdQiwi.ForeColor = System.Drawing.Color.Gray;
            this.IdQiwi.Location = new System.Drawing.Point(125, 340);
            this.IdQiwi.Name = "IdQiwi";
            this.IdQiwi.Size = new System.Drawing.Size(183, 31);
            this.IdQiwi.TabIndex = 190;
            this.IdQiwi.TabStop = false;
            this.IdQiwi.Text = "Id";
            this.IdQiwi.Click += new System.EventHandler(this.IdQiwi_Click);
            this.IdQiwi.TextChanged += new System.EventHandler(this.IdQiwi_TextChanged);
            this.IdQiwi.Leave += new System.EventHandler(this.IdQiwi_Leave);
            // 
            // QiwiClose
            // 
            this.QiwiClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.QiwiClose.Image = global::Bank.Properties.Resources.Close;
            this.QiwiClose.Location = new System.Drawing.Point(384, 6);
            this.QiwiClose.Margin = new System.Windows.Forms.Padding(2);
            this.QiwiClose.Name = "QiwiClose";
            this.QiwiClose.Size = new System.Drawing.Size(40, 40);
            this.QiwiClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.QiwiClose.TabIndex = 54;
            this.QiwiClose.TabStop = false;
            this.QiwiClose.Click += new System.EventHandler(this.QiwiClose_Click);
            // 
            // QiwiBack
            // 
            this.QiwiBack.BackColor = System.Drawing.SystemColors.Window;
            this.QiwiBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.QiwiBack.Image = global::Bank.Properties.Resources.back;
            this.QiwiBack.Location = new System.Drawing.Point(10, 6);
            this.QiwiBack.Margin = new System.Windows.Forms.Padding(2);
            this.QiwiBack.Name = "QiwiBack";
            this.QiwiBack.Size = new System.Drawing.Size(40, 40);
            this.QiwiBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.QiwiBack.TabIndex = 52;
            this.QiwiBack.TabStop = false;
            this.QiwiBack.Click += new System.EventHandler(this.QiwiBack_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(19, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(397, 37);
            this.label4.TabIndex = 1;
            this.label4.Text = "Поповнення рахунку Qiwi";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Bank.Properties.Resources.qiwi1;
            this.pictureBox1.Location = new System.Drawing.Point(133, 101);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(170, 175);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PayPalPanel
            // 
            this.PayPalPanel.Controls.Add(this.pictureBox11);
            this.PayPalPanel.Controls.Add(this.pictureBox10);
            this.PayPalPanel.Controls.Add(this.pictureBox8);
            this.PayPalPanel.Controls.Add(this.pictureBox7);
            this.PayPalPanel.Controls.Add(this.PayPalPay);
            this.PayPalPanel.Controls.Add(this.amountPayPal);
            this.PayPalPanel.Controls.Add(this.pictureBox5);
            this.PayPalPanel.Controls.Add(this.pictureBox6);
            this.PayPalPanel.Controls.Add(this.IdPayPal);
            this.PayPalPanel.Controls.Add(this.PayPalClose);
            this.PayPalPanel.Controls.Add(this.PayPalBack);
            this.PayPalPanel.Controls.Add(this.label5);
            this.PayPalPanel.Controls.Add(this.pictureBox9);
            this.PayPalPanel.Location = new System.Drawing.Point(-3, -1);
            this.PayPalPanel.Name = "PayPalPanel";
            this.PayPalPanel.Size = new System.Drawing.Size(442, 741);
            this.PayPalPanel.TabIndex = 195;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox11.Location = new System.Drawing.Point(-72, -317);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(756, 324);
            this.pictureBox11.TabIndex = 207;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox10.Location = new System.Drawing.Point(-24, 729);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(522, 250);
            this.pictureBox10.TabIndex = 206;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox8.Location = new System.Drawing.Point(-194, -37);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(202, 775);
            this.pictureBox8.TabIndex = 205;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox7.Location = new System.Drawing.Point(436, -21);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(351, 775);
            this.pictureBox7.TabIndex = 204;
            this.pictureBox7.TabStop = false;
            // 
            // PayPalPay
            // 
            this.PayPalPay.FlatAppearance.BorderSize = 0;
            this.PayPalPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PayPalPay.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PayPalPay.ForeColor = System.Drawing.Color.Gray;
            this.PayPalPay.Location = new System.Drawing.Point(95, 520);
            this.PayPalPay.Name = "PayPalPay";
            this.PayPalPay.Size = new System.Drawing.Size(266, 127);
            this.PayPalPay.TabIndex = 203;
            this.PayPalPay.Text = "Поповнити";
            this.PayPalPay.UseVisualStyleBackColor = true;
            this.PayPalPay.Click += new System.EventHandler(this.PayPalPay_Click);
            // 
            // amountPayPal
            // 
            this.amountPayPal.BackColor = System.Drawing.SystemColors.Window;
            this.amountPayPal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.amountPayPal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountPayPal.ForeColor = System.Drawing.Color.Gray;
            this.amountPayPal.Location = new System.Drawing.Point(148, 426);
            this.amountPayPal.Name = "amountPayPal";
            this.amountPayPal.Size = new System.Drawing.Size(147, 31);
            this.amountPayPal.TabIndex = 202;
            this.amountPayPal.TabStop = false;
            this.amountPayPal.Text = "Сума";
            this.amountPayPal.Click += new System.EventHandler(this.amountPayPal_Click);
            this.amountPayPal.TextChanged += new System.EventHandler(this.amountPayPal_TextChanged);
            this.amountPayPal.Leave += new System.EventHandler(this.amountPayPal_Leave);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pictureBox5.Location = new System.Drawing.Point(148, 463);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(147, 2);
            this.pictureBox5.TabIndex = 201;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pictureBox6.Location = new System.Drawing.Point(128, 373);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(183, 2);
            this.pictureBox6.TabIndex = 200;
            this.pictureBox6.TabStop = false;
            // 
            // IdPayPal
            // 
            this.IdPayPal.BackColor = System.Drawing.SystemColors.Window;
            this.IdPayPal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IdPayPal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.IdPayPal.ForeColor = System.Drawing.Color.Gray;
            this.IdPayPal.Location = new System.Drawing.Point(128, 343);
            this.IdPayPal.Name = "IdPayPal";
            this.IdPayPal.Size = new System.Drawing.Size(183, 31);
            this.IdPayPal.TabIndex = 199;
            this.IdPayPal.TabStop = false;
            this.IdPayPal.Text = "Id";
            this.IdPayPal.Click += new System.EventHandler(this.IdPayPal_Click);
            this.IdPayPal.TextChanged += new System.EventHandler(this.IdPayPal_TextChanged);
            this.IdPayPal.Leave += new System.EventHandler(this.IdPayPal_Leave);
            // 
            // PayPalClose
            // 
            this.PayPalClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PayPalClose.Image = global::Bank.Properties.Resources.Close;
            this.PayPalClose.Location = new System.Drawing.Point(387, 9);
            this.PayPalClose.Margin = new System.Windows.Forms.Padding(2);
            this.PayPalClose.Name = "PayPalClose";
            this.PayPalClose.Size = new System.Drawing.Size(40, 40);
            this.PayPalClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PayPalClose.TabIndex = 198;
            this.PayPalClose.TabStop = false;
            this.PayPalClose.Click += new System.EventHandler(this.PayPalClose_Click);
            // 
            // PayPalBack
            // 
            this.PayPalBack.BackColor = System.Drawing.SystemColors.Window;
            this.PayPalBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PayPalBack.Image = global::Bank.Properties.Resources.back;
            this.PayPalBack.Location = new System.Drawing.Point(17, 6);
            this.PayPalBack.Margin = new System.Windows.Forms.Padding(2);
            this.PayPalBack.Name = "PayPalBack";
            this.PayPalBack.Size = new System.Drawing.Size(40, 40);
            this.PayPalBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PayPalBack.TabIndex = 197;
            this.PayPalBack.TabStop = false;
            this.PayPalBack.Click += new System.EventHandler(this.PayPalBack_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(7, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(437, 37);
            this.label5.TabIndex = 196;
            this.label5.Text = "Поповнення рахунку PayPal";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Bank.Properties.Resources.paypal;
            this.pictureBox9.Location = new System.Drawing.Point(136, 104);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(170, 175);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox12.Location = new System.Drawing.Point(-73, -286);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(756, 291);
            this.pictureBox12.TabIndex = 211;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox13.Location = new System.Drawing.Point(-25, 762);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(522, 250);
            this.pictureBox13.TabIndex = 210;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox14.Location = new System.Drawing.Point(-198, -4);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(202, 775);
            this.pictureBox14.TabIndex = 209;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox15.Location = new System.Drawing.Point(435, 12);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(351, 775);
            this.pictureBox15.TabIndex = 208;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox20.Location = new System.Drawing.Point(-3, 727);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(455, 135);
            this.pictureBox20.TabIndex = 212;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox21.Location = new System.Drawing.Point(432, -4);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(19, 745);
            this.pictureBox21.TabIndex = 213;
            this.pictureBox21.TabStop = false;
            // 
            // InternetPayments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(436, 731);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.QiwiPanel);
            this.Controls.Add(this.PayPalPanel);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.PayPal);
            this.Controls.Add(this.Qiwi);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InternetPayments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.InternetPayments_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PayPal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qiwi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Closebtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.QiwiPanel.ResumeLayout(false);
            this.QiwiPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QiwiClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QiwiBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PayPalPanel.ResumeLayout(false);
            this.PayPalPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayPalClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayPalBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox Closebtn;
        private System.Windows.Forms.PictureBox Qiwi;
        private System.Windows.Forms.PictureBox PayPal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel QiwiPanel;
        private System.Windows.Forms.PictureBox QiwiClose;
        private System.Windows.Forms.PictureBox QiwiBack;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox IdQiwi;
        private System.Windows.Forms.TextBox AmountQiwi;
        public System.Windows.Forms.Button payQiwi;
        private System.Windows.Forms.Panel PayPalPanel;
        public System.Windows.Forms.Button PayPalPay;
        private System.Windows.Forms.TextBox amountPayPal;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox IdPayPal;
        private System.Windows.Forms.PictureBox PayPalClose;
        private System.Windows.Forms.PictureBox PayPalBack;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
    }
}