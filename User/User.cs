﻿using System.Data.SqlClient;

namespace UserClass
{
    public class User
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Nikita\Desktop\Course\Main\Bank\Bank.mdf;Integrated Security=True";
        protected int Id;

        protected long UserPhone;

        protected string UserPassword;

        protected string UserCard;

        protected string UserEmail;

        protected string UserName;

        protected string UserSurname;

        protected string UserPatronym;

        protected float UAH;


        public long userPhone
        {
            set
            {
                UserPhone = value;
            }

            get
            {
                return UserPhone;
            }
        }
        public string userPassword
        {
            set
            {
                UserPassword = value;
            }

            get
            {
                return UserPassword;
            }
        }
        public string userEmail
        {
            set
            {
                UserEmail = value;
            }

            get
            {
                return UserEmail;
            }
        }
        public string userCard
        {
            set
            {
                UserCard = value;
            }

            get
            {
                return UserCard;
            }
        }
        public string userName
        {
            set
            {
                UserName = value;
            }

            get
            {
                return UserName;
            }
        }
        public string userSurname
        {
            set
            {
                UserSurname = value;
            }

            get
            {
                return UserSurname;
            }
        }
        public string userPatronym
        {
            set
            {
                UserPatronym = value;
            }

            get
            {
                return UserPatronym;
            }
        }

        public float uah
        {
            set
            {
                UAH = value;
            }

            get
            {
                return UAH;
            }
        }

        public virtual void UserAuth(long login, string pass)
        {
            string query = "Select [Phone], [Password] from Users where Phone = '" + login + "' and Password = '" + pass + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    counter++;
                }
            }
        }
        public void UserConnection(long login)
        {
            string query = "Select [Name], [Surname], [Patronym],[UAH], [Password], [CardNumber], [Email] from Users where Phone = '" + login + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    userPassword = Reader["Password"].ToString();
                    userName = Reader["Name"].ToString();
                    userSurname = Reader["Surname"].ToString();
                    userCard = Reader["CardNumber"].ToString();
                    userPatronym = Reader["Patronym"].ToString();
                    userEmail = Reader["Email"].ToString();
                    uah = float.Parse(Reader["UAH"].ToString());
                }
            }
        }
        public int counter;
        public virtual void UserExist(long login)
        {
            counter = 1;
            string query = "Select [Phone] from Users where Phone = '" + login + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {

                    counter = 0;
                }
            }
        }
        public int cardCounter;
        public virtual void UserExist(string card)
        {
            cardCounter = 1;
            string query = "Select [CardNumber] from Users where CardNumber = '" + card + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {

                    cardCounter = 0;
                }
            }
        }
        public virtual void UserAuthing(long login, string pass)
        {
            string query = "Select [Phone], [Password] from Users where Phone = '" + login + "' and Password = '" + pass + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    counter++;
                }
            }
        }

        public virtual void UserAuthing(long login)
        {
            string query = "Select [Phone] from Table where Phone = '" + login + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    counter++;
                }
            }
        }

        public virtual void UpdateUserInfo(string newPass, long login)
        {
            string query = "Update Table set Password = '" + newPass + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }


        public virtual void UpdateUserInfo(string newEmail, string login)
        {
            string query = "Update Table set Email = '" + newEmail + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateUserInfo(long newPhone, long login)
        {
            string query = "Update Table set Phone = '" + newPhone + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }

        public virtual void NewUser(string name, string surname, string patronym, string card, string email, long phone, string password)
        {
            int id;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmdzs = new SqlCommand("SELECT MAX (Id) as max_Id FROM Users", connection);
                connection.Open();
                id = (int)cmdzs.ExecuteScalar();
            }
            string query = "INSERT into Users (Id, Phone, CardNumber, Password, Name, Surname, Patronym, UAH, Email) VALUES (@id, @phone,@card, @password, @name, @surname, @patronym, @uah, @email); SELECT CAST(scope_identity() AS int)";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, connection);
                Command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id + 1;
                Command.Parameters.Add("@phone", System.Data.SqlDbType.VarChar).Value = phone;
                Command.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = password;
                Command.Parameters.Add("@email", System.Data.SqlDbType.NVarChar).Value = email;
                Command.Parameters.Add("@card", System.Data.SqlDbType.VarChar).Value = card;
                Command.Parameters.Add("@name", System.Data.SqlDbType.NVarChar).Value = name;
                Command.Parameters.Add("@surname", System.Data.SqlDbType.NVarChar).Value = surname;
                Command.Parameters.Add("@patronym", System.Data.SqlDbType.NVarChar).Value = patronym;
                Command.Parameters.Add("@uah", System.Data.SqlDbType.Decimal).Value = 0;


                connection.Open();
                Command.ExecuteNonQuery();
            }
        }

    }
}
