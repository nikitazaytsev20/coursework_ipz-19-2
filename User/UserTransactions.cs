﻿using System.Data;
using System.Data.SqlClient;

namespace UserClass
{
    public class UserTransactions : User
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Nikita\Desktop\Course\Main\Bank\Bank.mdf;Integrated Security=True";

        public int operationSuccsess = 0;

        protected float RecipientBalance;

        protected string RecipientName;

        protected string RecipientSurname;

        protected string RecipientPatronymic;

        protected string RecipientCard;

        protected string RecipientEmail;

        public string recipientCard
        {
            set
            {
                RecipientCard = value;
            }

            get
            {
                return RecipientCard;
            }
        }

        public string recipientName
        {
            set
            {
                RecipientName = value;
            }

            get
            {
                return RecipientName;
            }
        }

        public string recipientSurname
        {
            set
            {
                RecipientSurname = value;
            }

            get
            {
                return RecipientSurname;
            }
        }

        public string recipientPatronymic
        {
            set
            {
                RecipientPatronymic = value;
            }

            get
            {
                return RecipientPatronymic;
            }
        }

        public float recipientBalance
        {
            set
            {
                RecipientBalance = value;
            }

            get
            {
                return RecipientBalance;
            }
        }

        public string recipientEmail
        {
            set
            {
                RecipientEmail = value;
            }

            get
            {
                return RecipientEmail;
            }
        }

        public void UserTransfer(long login, float amount)
        {
            string query = "Update Users set UAH = @uah where Phone = @login";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand userCommand = new SqlCommand(query, connection);
                userCommand.Parameters.Add("@uah", SqlDbType.Decimal).Value = amount;
                userCommand.Parameters.Add("@login", SqlDbType.BigInt).Value = login;
                connection.Open();
                userCommand.ExecuteNonQuery();
            }
        }

        public void RecipientTransfer(string recipientCard, float amount)
        {
            string query = "Update Users set UAH = @recipientBalance where CardNumber = '" + recipientCard + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand recipientCommand = new SqlCommand(query, connection);
                recipientCommand.Parameters.Add("@recipientBalance", SqlDbType.Decimal).Value = amount;
                connection.Open();
                recipientCommand.ExecuteNonQuery();
            }
        }

        public void SenderLoading(long login)
        {
            string query = "Select [Name], [Surname], [Patronym], [UAH], [Password], [CardNumber], [Email] from Users where Phone = '" + login + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    userPassword = Reader["Password"].ToString();
                    userName = Reader["Name"].ToString();
                    userSurname = Reader["Surname"].ToString();
                    userPatronym = Reader["Patronym"].ToString();
                    userCard = Reader["CardNumber"].ToString();
                    uah = float.Parse(Reader["UAH"].ToString());
                    userEmail = Reader["Email"].ToString();
                }
            }
        }

        public void RecipientLoading(string recipientCard)
        {
            string query = "Select [Name], [Surname], [Patronym], [UAH], [Email] from Users where CardNumber = '" + recipientCard + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    operationSuccsess++;
                    recipientName = Reader["Name"].ToString();
                    recipientSurname = Reader["Surname"].ToString();
                    recipientPatronymic = Reader["Patronym"].ToString();
                    recipientBalance = float.Parse(Reader["UAH"].ToString());
                    recipientEmail = Reader["Email"].ToString();
                }
            }
        }

    }
}
