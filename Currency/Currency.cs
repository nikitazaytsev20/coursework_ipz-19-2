﻿using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Currency
{
    public class CurrencyClass
    {
        private readonly string Link = "https://privatbank.ua/";

        public string buyUsd { private set; get; }
        public string sellUsd { private set; get; }
        public string buyEur { private set; get; }
        public string sellEur { private set; get; }
        public string buyRub { private set; get; }
        public string sellRub { private set; get; }

        protected float BuyUsd;

        protected float SellUsd;

        protected float BuyEur;

        protected float SellEur;

        protected float BuyRub;

        protected float SellRub;
        public float toBuyUsd
        {
            private set
            {
                BuyUsd = value;
            }

            get
            {
                return BuyUsd;
            }
        }

        public float toSellUsd
        {
            private set
            {
                SellUsd = value;
            }

            get
            {
                return SellUsd;
            }
        }

        public float toBuyEur
        {
            private set
            {
                BuyEur = value;
            }

            get
            {
                return BuyEur;
            }
        }

        public float toSellEur
        {
            private set
            {
                SellEur = value;
            }
            get
            {
                return SellEur;
            }
        }

        public float toBuyRub
        {
            private set
            {
                BuyRub = value;
            }
            get
            {
                return BuyRub;
            }
        }

        public float toSellRub
        {
            private set
            {
                SellRub = value;
            }
            get
            {
                return SellRub;
            }
        }
        public CurrencyClass()
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                WebClient src = new WebClient();
                src.Encoding = Encoding.UTF8;
                string link = src.DownloadString(Link);
                File.WriteAllText("currency.txt", link);
                buyUsd = Regex.Match(link, "<td\\s*id=\"USD_buy\">\\s*(\\d*\\.\\d*)\\s*<\\/td>").Groups[1].Value.Replace(".", ",");
                toBuyUsd = float.Parse(buyUsd);
                sellUsd = Regex.Match(link, "<td\\s*id=\"USD_sell\">\\s*(\\d*\\.\\d*)\\s*<\\/td>").Groups[1].Value.Replace(".", ",");
                toSellUsd = float.Parse(sellUsd);
                buyEur = Regex.Match(link, "<td\\s*id=\"EUR_buy\">\\s*(\\d*\\.\\d*)\\s*<\\/td>").Groups[1].Value.Replace(".", ",");
                toBuyEur = float.Parse(buyEur);
                sellEur = Regex.Match(link, "<td\\s*id=\"EUR_sell\">\\s*(\\d*\\.\\d*)\\s*<\\/td>").Groups[1].Value.Replace(".", ",");
                toSellEur = float.Parse(sellEur);
                buyRub = Regex.Match(link, "<td\\s*id=\"RUB_buy\">\\s*(\\d*\\.\\d*)\\s*<\\/td>").Groups[1].Value.Replace(".", ",");
                toBuyRub = float.Parse(buyRub);
                sellRub = Regex.Match(link, "<td\\s*id=\"RUB_sell\">\\s*(\\d*\\.\\d*)\\s*<\\/td>").Groups[1].Value.Replace(".", ",");
                toSellRub = float.Parse(sellRub);
            }
        }
    }
}
